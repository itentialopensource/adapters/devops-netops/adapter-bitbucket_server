
## 1.0.3 [05-30-2023]

* Add get file streams with query

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!7

---

## 1.0.2 [03-09-2023]

* ADAPT-2560-Add support for editting multiple files

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!6

---

## 1.0.1 [02-27-2023]

* Update getgetcontent task to take in query options

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!5

---

## 1.0.0 [09-09-2022]

* Update call names

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!4

---