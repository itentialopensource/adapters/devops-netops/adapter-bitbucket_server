## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Bitbucket Server. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Bitbucket Server.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Bitbucket_server. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(callback)</td>
    <td style="padding:15px">getUsers</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/users?{query} <br /> v1:{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsers(body, callback)</td>
    <td style="padding:15px">updateUserDetails</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/users?{query} <br /> v1:{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAvatar(userSlug, callback)</td>
    <td style="padding:15px">deleteAvatar</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/users/{pathv1}/avatar.png?{query} <br /> v1:{base_path}/{version}/users/{pathv1}/avatar.png?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadAvatar(userSlug, callback)</td>
    <td style="padding:15px">uploadAvatar</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/users/{pathv1}/avatar.png?{query} <br /> v1:{base_path}/{version}/users/{pathv1}/avatar.png?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUser(userSlug, callback)</td>
    <td style="padding:15px">getUser</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/users/{pathv1}?{query} <br /> v1:{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersCredentials(body, callback)</td>
    <td style="padding:15px">updateUserPassword</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/users/credentials?{query} <br /> v1:{base_path}/{version}/users/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRefChangeActivity(ref, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getRefChangeActivity</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/ref-change-activities?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/ref-change-activities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findBranches(filterText, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">findBranches</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/ref-change-activities/branches?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/ref-change-activities/branches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReviewerGroups(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getReviewerGroups</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/reviewer-groups?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/reviewer-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposSettingsReviewerGroups(projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">create</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/reviewer-groups?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/reviewer-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetUsers(id, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getUsers</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/reviewer-groups/{pathv3}/users?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/reviewer-groups/{pathv3}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReviewerGroup(id, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getReviewerGroup</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/reviewer-groups/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/reviewer-groups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">update(id, projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">update</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/reviewer-groups/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/reviewer-groups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delete(id, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">delete</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/reviewer-groups/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/reviewer-groups/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAll(filter, callback)</td>
    <td style="padding:15px">getAll</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/rate-limit/settings/users?{query} <br /> v1:{base_path}/{version}/admin/rate-limit/settings/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">set(body, callback)</td>
    <td style="padding:15px">set</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/rate-limit/settings/users?{query} <br /> v1:{base_path}/{version}/admin/rate-limit/settings/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">get(userSlug, callback)</td>
    <td style="padding:15px">get</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/rate-limit/settings/users/{pathv1}?{query} <br /> v1:{base_path}/{version}/admin/rate-limit/settings/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAdminRateLimitSettingsUsersWithuserSlug(userSlug, callback)</td>
    <td style="padding:15px">delete</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/rate-limit/settings/users/{pathv1}?{query} <br /> v1:{base_path}/{version}/admin/rate-limit/settings/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAdminRateLimitSettingsUsersWithuserSlug(userSlug, body, callback)</td>
    <td style="padding:15px">set</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/rate-limit/settings/users/{pathv1}?{query} <br /> v1:{base_path}/{version}/admin/rate-limit/settings/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBuildCapabilities(callback)</td>
    <td style="padding:15px">getCapabilities</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/build/capabilities?{query} <br /> v1:{base_path}/{version}/build/capabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPullRequestTasks(projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">getPullRequestTasks</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/tasks?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">countPullRequestTasks(projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">countPullRequestTasks</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/tasks/count?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/tasks/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">streamFiles(at, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">streamFiles</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/files?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getstreamFiles(at, projectKey, repositorySlug, uriPath, callback)</td>
    <td style="padding:15px">streamFiles</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/files/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/files/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getstreamFilesWithQuery(projectKey, repositorySlug, uriPath, query, callback)</td>
    <td style="padding:15px">streamFiles</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/files/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/files/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listParticipants(pullRequestId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">listParticipants</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/participants?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/participants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignParticipantRole(username, pullRequestId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">unassignParticipantRole</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/participants?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/participants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignParticipantRole(pullRequestId, projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">assignParticipantRole</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/participants?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/participants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateStatus(userSlug, pullRequestId, projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">updateStatus</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/participants/{pathv4}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/participants/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteunassignParticipantRole(userSlug, pullRequestId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">unassignParticipantRole</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/participants/{pathv4}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/participants/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoriesRecentlyAccessed(permission, callback)</td>
    <td style="padding:15px">getRepositoriesRecentlyAccessed</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/profile/recent/repos?{query} <br /> v1:{base_path}/{version}/profile/recent/repos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">withdrawApproval(pullRequestId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">withdrawApproval</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/approve?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/approve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">approve(pullRequestId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">approve</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/approve?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/approve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">streamChanges(changeScope, sinceId, untilId, withComments, projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">streamChanges</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/changes?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/changes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommits(followRenames, ignoreMissing, merges, pathParam, since, until, withCounts, avatarSize, avatarScheme, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getCommits</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCommit(pathParam, commitId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getCommit</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAttachment(attachmentId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">deleteAttachment</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/attachments/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/attachments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttachment(attachmentId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getAttachment</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/attachments/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/attachments/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAttachmentMetadata(attachmentId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">deleteAttachmentMetadata</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/attachments/{pathv3}/metadata?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/attachments/{pathv3}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAttachmentMetadata(attachmentId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getAttachmentMetadata</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/attachments/{pathv3}/metadata?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/attachments/{pathv3}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveAttachmentMetadata(attachmentId, projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">saveAttachmentMetadata</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/attachments/{pathv3}/metadata?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/attachments/{pathv3}/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">streamRawDiff(contextLines, since, srcPath, until, whitespace, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">streamRawDiff</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/diff?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/diff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposDiffWithpath(contextLines, since, srcPath, until, whitespace, projectKey, repositorySlug, uriPath, callback)</td>
    <td style="padding:15px">streamRawDiff</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/diff/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/diff/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposCommitsPullRequests(commitId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getPullRequests</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/pull-requests?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/pull-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getstreamChanges(from, to, fromRepo, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">streamChanges</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/compare/changes?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/compare/changes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getstreamDiff1(from, to, fromRepo, srcPath, contextLines, whitespace, projectKey, repositorySlug, uriPath, callback)</td>
    <td style="padding:15px">streamDiff</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/compare/diff{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/compare/diff{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">streamCommits(from, to, fromRepo, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">streamCommits</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/compare/commits?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/compare/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposPullRequestsBlockerComments(projectKey, repositorySlug, pullRequestId, body, callback)</td>
    <td style="padding:15px">createComment</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/blocker-comments?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/blocker-comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposPullRequestsBlockerComments(count, state, projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">getComments</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/blocker-comments?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/blocker-comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposPullRequestsBlockerCommentsWithcommentId(version, commentId, projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">deleteComment</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/blocker-comments/{pathv4}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/blocker-comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putReposPullRequestsBlockerCommentsWithcommentId(commentId, projectKey, repositorySlug, pullRequestId, body, callback)</td>
    <td style="padding:15px">updateComment</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/blocker-comments/{pathv4}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/blocker-comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposPullRequestsBlockerCommentsWithcommentId(commentId, projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">getComment</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/blocker-comments/{pathv4}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/blocker-comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposSettingsAutoDecline(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">get</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/auto-decline?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/auto-decline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposSettingsAutoDecline(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">delete</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/auto-decline?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/auto-decline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putReposSettingsAutoDecline(projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">set</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/auto-decline?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/auto-decline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInformation(callback)</td>
    <td style="padding:15px">getInformation</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/cluster?{query} <br /> v1:{base_path}/{version}/admin/cluster?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoryHooks(type, projectKey, callback)</td>
    <td style="padding:15px">getRepositoryHooks</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/hooks?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSettings(hookKey, projectKey, body, callback)</td>
    <td style="padding:15px">setSettings</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/hooks/{pathv2}/settings?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/hooks/{pathv2}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSettingsHooksSettings(hookKey, projectKey, callback)</td>
    <td style="padding:15px">getSettings</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/hooks/{pathv2}/settings?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/hooks/{pathv2}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositoryHook(hookKey, projectKey, callback)</td>
    <td style="padding:15px">getRepositoryHook</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/hooks/{pathv2}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/hooks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableHook(hookKey, projectKey, callback)</td>
    <td style="padding:15px">enableHook</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/hooks/{pathv2}/enabled?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/hooks/{pathv2}/enabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableHook(hookKey, projectKey, callback)</td>
    <td style="padding:15px">disableHook</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/hooks/{pathv2}/enabled?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/hooks/{pathv2}/enabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminPermissionsGroups(filter, callback)</td>
    <td style="padding:15px">getGroupsWithAnyPermission</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/permissions/groups?{query} <br /> v1:{base_path}/{version}/admin/permissions/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setPermissionForGroups(permission, name, callback)</td>
    <td style="padding:15px">setPermissionForGroups</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/permissions/groups?{query} <br /> v1:{base_path}/{version}/admin/permissions/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokePermissionsForGroup(name, callback)</td>
    <td style="padding:15px">revokePermissionsForGroup</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/permissions/groups?{query} <br /> v1:{base_path}/{version}/admin/permissions/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminPermissionsGroupsNone(filter, callback)</td>
    <td style="padding:15px">getGroupsWithoutAnyPermission</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/permissions/groups/none?{query} <br /> v1:{base_path}/{version}/admin/permissions/groups/none?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminPermissionsUsers(filter, callback)</td>
    <td style="padding:15px">getUsersWithAnyPermission</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/permissions/users?{query} <br /> v1:{base_path}/{version}/admin/permissions/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setPermissionForUsers(name, permission, callback)</td>
    <td style="padding:15px">setPermissionForUsers</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/permissions/users?{query} <br /> v1:{base_path}/{version}/admin/permissions/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokePermissionsForUser(name, callback)</td>
    <td style="padding:15px">revokePermissionsForUser</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/permissions/users?{query} <br /> v1:{base_path}/{version}/admin/permissions/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersWithoutAnyPermission(filter, callback)</td>
    <td style="padding:15px">getUsersWithoutAnyPermission</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/permissions/users/none?{query} <br /> v1:{base_path}/{version}/admin/permissions/users/none?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMergeConfig(scmId, callback)</td>
    <td style="padding:15px">getMergeConfig</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/pull-requests/{pathv1}?{query} <br /> v1:{base_path}/{version}/admin/pull-requests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setMergeConfig(scmId, body, callback)</td>
    <td style="padding:15px">setMergeConfig</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/pull-requests/{pathv1}?{query} <br /> v1:{base_path}/{version}/admin/pull-requests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPage(direction, at, state, order, withAttributes, withProperties, filterText, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getPage</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposPullRequests(projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">create1</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposPullRequestsWithpullRequestIdDiff(contextLines, whitespace, projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">streamRawDiff</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}.diff?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}.diff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reopen(version, pullRequestId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">reopen</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/reopen?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/reopen?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">streamPatch(projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">streamPatch</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}.patch?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}.patch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActivities(fromId, fromType, pullRequestId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getActivities</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/activities?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/activities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">canMerge(pullRequestId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">canMerge</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/merge?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">merge(version, pullRequestId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">merge</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/merge?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">decline(version, pullRequestId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">decline</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/decline?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/decline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposPullRequestsWithpullRequestId(pullRequestId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">get</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putReposPullRequestsWithpullRequestId(pullRequestId, projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">update</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposPullRequestsWithpullRequestId(pullRequestId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">delete</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBanner(callback)</td>
    <td style="padding:15px">getBanner</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/banner?{query} <br /> v1:{base_path}/{version}/admin/banner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBanner(callback)</td>
    <td style="padding:15px">deleteBanner</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/banner?{query} <br /> v1:{base_path}/{version}/admin/banner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setBanner(body, callback)</td>
    <td style="padding:15px">setBanner</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/banner?{query} <br /> v1:{base_path}/{version}/admin/banner?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unwatch(commitId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">unwatch</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/watch?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">watch(commitId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">watch</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/watch?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putsetSettings(body, callback)</td>
    <td style="padding:15px">setSettings</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/rate-limit/settings?{query} <br /> v1:{base_path}/{version}/admin/rate-limit/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetSettings(callback)</td>
    <td style="padding:15px">getSettings</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/rate-limit/settings?{query} <br /> v1:{base_path}/{version}/admin/rate-limit/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChanges(since, withComments, commitId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getChanges</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/changes?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/changes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminDefaultBranch(callback)</td>
    <td style="padding:15px">getDefaultBranch</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/default-branch?{query} <br /> v1:{base_path}/{version}/admin/default-branch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setDefaultBranch(body, callback)</td>
    <td style="padding:15px">setDefaultBranch</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/default-branch?{query} <br /> v1:{base_path}/{version}/admin/default-branch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearDefaultBranch(callback)</td>
    <td style="padding:15px">clearDefaultBranch</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/default-branch?{query} <br /> v1:{base_path}/{version}/admin/default-branch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposPullRequestsDiff(contextLines, sinceId, srcPath, untilId, whitespace, projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">streamRawDiff</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/diff?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/diff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposPullRequestsDiffWithpath(contextLines, sinceId, srcPath, untilId, whitespace, projectKey, repositorySlug, pullRequestId, uriPath, callback)</td>
    <td style="padding:15px">streamRawDiff</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/diff/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/diff/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">streamRaw(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">streamRaw</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/raw?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/raw?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getstreamRaw(at, hardwrap, htmlEscape, includeHeadingId, markup, projectKey, repositorySlug, uriPath, callback)</td>
    <td style="padding:15px">streamRaw</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/raw/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/raw/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">add(commitId, projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">add</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/builds?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/builds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposCommitsBuilds(key, commitId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">get</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/builds?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/builds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposCommitsBuilds(key, commitId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">delete</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/builds?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/builds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRepository(projectKey, body, callback)</td>
    <td style="padding:15px">createRepository</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepositories(projectKey, callback)</td>
    <td style="padding:15px">getRepositories</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetDefaultBranch(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getDefaultBranch</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/default-branch?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/default-branch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putsetDefaultBranch(projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">setDefaultBranch</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/default-branch?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/default-branch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepository(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">deleteRepository</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forkRepository(projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">forkRepository</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRepository(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getRepository</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRepository(projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">updateRepository</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findContributing(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">findContributing</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/contributing?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/contributing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findLicense(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">findLicense</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/license?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findReadme(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">findReadme</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/readme?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/readme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForkedRepositories(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getForkedRepositories</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/forks?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/forks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRelatedRepositories(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getRelatedRepositories</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/related?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/related?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retryCreateRepository(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">retryCreateRepository</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/recreate?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/recreate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposWatch(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">unwatch</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/watch?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposWatch(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">watch</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/watch?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvatar(version, hookKey, callback)</td>
    <td style="padding:15px">getAvatar</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/hooks/{pathv1}/avatar?{query} <br /> v1:{base_path}/{version}/hooks/{pathv1}/avatar?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">search(direction, filter, role, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">search</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/participants?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/participants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposPullRequestsWatch(projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">unwatch</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/watch?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposPullRequestsWatch(projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">watch</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/watch?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/watch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContent(at, size, type, blame, noContent, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getContent</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/browse?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/browse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editFile(projectKey, repositorySlug, uriPath, branch, content, message, sourceBranch, sourceCommitId, callback)</td>
    <td style="padding:15px">editFile</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/browse/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/browse/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetContentWithOptions(queryData, projectKey, repositorySlug, uriPath, callback)</td>
    <td style="padding:15px">getgetContentWithOptions</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/browse/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/browse/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPullRequestSuggestions(changesSince, limit, callback)</td>
    <td style="padding:15px">getPullRequestSuggestions</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/dashboard/pull-request-suggestions?{query} <br /> v1:{base_path}/{version}/dashboard/pull-request-suggestions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetPullRequests(state, role, participantStatus, order, closedSince, callback)</td>
    <td style="padding:15px">getPullRequests</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/dashboard/pull-requests?{query} <br /> v1:{base_path}/{version}/dashboard/pull-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPullRequestCount(callback)</td>
    <td style="padding:15px">getPullRequestCount</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/inbox/pull-requests/count?{query} <br /> v1:{base_path}/{version}/inbox/pull-requests/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInboxPullRequests(start, limit, role, callback)</td>
    <td style="padding:15px">getPullRequests</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/inbox/pull-requests?{query} <br /> v1:{base_path}/{version}/inbox/pull-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposPullRequestsComments(projectKey, repositorySlug, pullRequestId, body, callback)</td>
    <td style="padding:15px">createComment1</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/comments?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetComments(anchorState, diffType, fromHash, pathParam, state, toHash, projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">getComments</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/comments?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposPullRequestsCommentsWithcommentId(version, commentId, projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">deleteComment1</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/comments/{pathv4}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putReposPullRequestsCommentsWithcommentId(commentId, projectKey, repositorySlug, pullRequestId, body, callback)</td>
    <td style="padding:15px">updateComment1</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/comments/{pathv4}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposPullRequestsCommentsWithcommentId(commentId, projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">getComment1</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/comments/{pathv4}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applySuggestion(projectKey, repositorySlug, pullRequestId, commentId, body, callback)</td>
    <td style="padding:15px">applySuggestion</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/comments/{pathv4}/apply-suggestion?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/comments/{pathv4}/apply-suggestion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findWebhooks(event, statistics, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">findWebhooks</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/webhooks?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWebhook(projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">createWebhook</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/webhooks?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/webhooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebhook(webhookId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">deleteWebhook</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/webhooks/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/webhooks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebhook(statistics, webhookId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getWebhook</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/webhooks/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/webhooks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWebhook(webhookId, projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">updateWebhook</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/webhooks/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/webhooks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLatestInvocation(event, outcome, webhookId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getLatestInvocation</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/webhooks/{pathv3}/latest?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/webhooks/{pathv3}/latest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatistics(event, webhookId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getStatistics</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/webhooks/{pathv3}/statistics?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/webhooks/{pathv3}/statistics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatisticsSummary(webhookId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getStatisticsSummary</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/webhooks/{pathv3}/statistics/summary?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/webhooks/{pathv3}/statistics/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testWebhook(url, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">testWebhook</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/webhooks/test?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/webhooks/test?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetChanges(since, until, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getChanges</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/changes?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/changes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBranches(base, details, filterText, orderBy, boostMatches, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getBranches</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/branches?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/branches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createBranch(projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">createBranch</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/branches?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/branches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposBranchesDefault(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getDefaultBranch1</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/branches/default?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/branches/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setDefaultBranch1(projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">setDefaultBranch1</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/branches/default?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/branches/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelImportJob(jobId, callback)</td>
    <td style="padding:15px">cancelImportJob</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/migration/imports/{pathv1}/cancel?{query} <br /> v1:{base_path}/{version}/migration/imports/{pathv1}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExportJob(jobId, callback)</td>
    <td style="padding:15px">getExportJob</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/migration/exports/{pathv1}?{query} <br /> v1:{base_path}/{version}/migration/exports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelExportJob(jobId, callback)</td>
    <td style="padding:15px">cancelExportJob</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/migration/exports/{pathv1}/cancel?{query} <br /> v1:{base_path}/{version}/migration/exports/{pathv1}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExportJobMessages(severity, subject, jobId, callback)</td>
    <td style="padding:15px">getExportJobMessages</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/migration/exports/{pathv1}/messages?{query} <br /> v1:{base_path}/{version}/migration/exports/{pathv1}/messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImportJobMessages(severity, subject, jobId, callback)</td>
    <td style="padding:15px">getImportJobMessages</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/migration/imports/{pathv1}/messages?{query} <br /> v1:{base_path}/{version}/migration/imports/{pathv1}/messages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">previewExport(body, callback)</td>
    <td style="padding:15px">previewExport</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/migration/exports/preview?{query} <br /> v1:{base_path}/{version}/migration/exports/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startExport(body, callback)</td>
    <td style="padding:15px">startExport</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/migration/exports?{query} <br /> v1:{base_path}/{version}/migration/exports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startImport(body, callback)</td>
    <td style="padding:15px">startImport</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/migration/imports?{query} <br /> v1:{base_path}/{version}/migration/imports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImportJob(jobId, callback)</td>
    <td style="padding:15px">getImportJob</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/migration/imports/{pathv1}?{query} <br /> v1:{base_path}/{version}/migration/imports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposCommitsDiff(autoSrcPath, contextLines, since, srcPath, whitespace, commitId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">streamRawDiff</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/diff?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/diff?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposCommitsDiffWithpath(autoSrcPath, contextLines, since, srcPath, whitespace, commitId, projectKey, repositorySlug, uriPath, callback)</td>
    <td style="padding:15px">streamRawDiff</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/diff/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/diff/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setRootLevel(levelName, callback)</td>
    <td style="padding:15px">setRootLevel</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/logs/rootLogger/{pathv1}?{query} <br /> v1:{base_path}/{version}/logs/rootLogger/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRootLevel(callback)</td>
    <td style="padding:15px">getRootLevel</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/logs/rootLogger?{query} <br /> v1:{base_path}/{version}/logs/rootLogger?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setLevel(levelName, loggerName, callback)</td>
    <td style="padding:15px">setLevel</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/logs/logger/{pathv1}/{pathv2}?{query} <br /> v1:{base_path}/{version}/logs/logger/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLevel(loggerName, callback)</td>
    <td style="padding:15px">getLevel</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/logs/logger/{pathv1}?{query} <br /> v1:{base_path}/{version}/logs/logger/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLabels(prefix, callback)</td>
    <td style="padding:15px">getLabels</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/labels?{query} <br /> v1:{base_path}/{version}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLabelsLabeled(type, labelName, callback)</td>
    <td style="padding:15px">getLabelables</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/labels/{pathv1}/labeled?{query} <br /> v1:{base_path}/{version}/labels/{pathv1}/labeled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLabelsWithlabelName(labelName, callback)</td>
    <td style="padding:15px">getLabel</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/labels/{pathv1}?{query} <br /> v1:{base_path}/{version}/labels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHistory(order, callback)</td>
    <td style="padding:15px">getHistory</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/rate-limit/history?{query} <br /> v1:{base_path}/{version}/admin/rate-limit/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createProject(body, callback)</td>
    <td style="padding:15px">createProject</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects?{query} <br /> v1:{base_path}/{version}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjects(name, permission, callback)</td>
    <td style="padding:15px">getProjects</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects?{query} <br /> v1:{base_path}/{version}/projects?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProjectAvatar(s, projectKey, callback)</td>
    <td style="padding:15px">getProjectAvatar</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/avatar.png?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/avatar.png?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postuploadAvatar(projectKey, callback)</td>
    <td style="padding:15px">uploadAvatar</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/avatar.png?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/avatar.png?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProject(projectKey, callback)</td>
    <td style="padding:15px">deleteProject</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateProject(projectKey, body, callback)</td>
    <td style="padding:15px">updateProject</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProject(projectKey, callback)</td>
    <td style="padding:15px">getProject</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetGroupsWithAnyPermission(filter, projectKey, callback)</td>
    <td style="padding:15px">getGroupsWithAnyPermission</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/permissions/groups?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/permissions/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putsetPermissionForGroups(permission, name, projectKey, callback)</td>
    <td style="padding:15px">setPermissionForGroups</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/permissions/groups?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/permissions/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionsGroups(name, projectKey, callback)</td>
    <td style="padding:15px">revokePermissionsForGroup</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/permissions/groups?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/permissions/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetGroupsWithoutAnyPermission(filter, projectKey, callback)</td>
    <td style="padding:15px">getGroupsWithoutAnyPermission</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/permissions/groups/none?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/permissions/groups/none?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetUsersWithAnyPermission(filter, projectKey, callback)</td>
    <td style="padding:15px">getUsersWithAnyPermission</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/permissions/users?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/permissions/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putsetPermissionForUsers(name, permission, projectKey, callback)</td>
    <td style="padding:15px">setPermissionForUsers</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/permissions/users?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/permissions/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePermissionsUsers(name, projectKey, callback)</td>
    <td style="padding:15px">revokePermissionsForUser</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/permissions/users?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/permissions/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersWithoutPermission(filter, projectKey, callback)</td>
    <td style="padding:15px">getUsersWithoutPermission</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/permissions/users/none?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/permissions/users/none?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hasAllUserPermission(permission, projectKey, callback)</td>
    <td style="padding:15px">hasAllUserPermission</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/permissions/{pathv2}/all?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/permissions/{pathv2}/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyAllUserPermission(allow, permission, projectKey, callback)</td>
    <td style="padding:15px">modifyAllUserPermission</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/permissions/{pathv2}/all?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/permissions/{pathv2}/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPullRequestSettings(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getPullRequestSettings</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/pull-requests?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/pull-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePullRequestSettings(projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">updatePullRequestSettings</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/pull-requests?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/pull-requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSettings(userSlug, body, callback)</td>
    <td style="padding:15px">updateSettings</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/users/{pathv1}/settings?{query} <br /> v1:{base_path}/{version}/users/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserSettings(userSlug, callback)</td>
    <td style="padding:15px">getUserSettings</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/users/{pathv1}/settings?{query} <br /> v1:{base_path}/{version}/users/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReview(projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">getReview</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/review?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/review?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discardReview(projectKey, repositorySlug, pullRequestId, callback)</td>
    <td style="padding:15px">discardReview</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/review?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/review?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">finishReview(projectKey, repositorySlug, pullRequestId, body, callback)</td>
    <td style="padding:15px">finishReview</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/review?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/review?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">preview(hardwrap, htmlEscape, includeHeadingId, urlMode, body, callback)</td>
    <td style="padding:15px">preview</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/markup/preview?{query} <br /> v1:{base_path}/{version}/markup/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetRepositoryHooks(type, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getRepositoryHooks</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/hooks?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putsetSettings1(hookKey, projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">setSettings</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/hooks/{pathv3}/settings?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/hooks/{pathv3}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposHooks(hookKey, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getSettings</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/hooks/{pathv3}/settings?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/hooks/{pathv3}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetRepositoryHook(hookKey, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getRepositoryHook</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/hooks/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/hooks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRepositoryHook(hookKey, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">deleteRepositoryHook</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/hooks/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/hooks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putenableHook(hookKey, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">enableHook</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/hooks/{pathv3}/enabled?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/hooks/{pathv3}/enabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletedisableHook(hookKey, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">disableHook</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/settings/hooks/{pathv3}/enabled?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/settings/hooks/{pathv3}/enabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeploymentCapabilities(callback)</td>
    <td style="padding:15px">getCapabilities1</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/deployment/capabilities?{query} <br /> v1:{base_path}/{version}/deployment/capabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetPullRequestSettings(scmId, projectKey, callback)</td>
    <td style="padding:15px">getPullRequestSettings</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/pull-requests/{pathv2}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/pull-requests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postupdatePullRequestSettings(scmId, projectKey, body, callback)</td>
    <td style="padding:15px">updatePullRequestSettings</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/pull-requests/{pathv2}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/pull-requests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTag(projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">createTag</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/tags?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTags(filterText, orderBy, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getTags</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/tags?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTag(projectKey, repositorySlug, name, callback)</td>
    <td style="padding:15px">getTag</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/tags/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/tags/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllLabelsForRepository(projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getAllLabelsForRepository</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/labels?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addLabel(projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">addLabel</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/labels?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/labels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeLabel(labelName, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">removeLabel</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/labels/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/labels/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposCommitsDeployments(key, environmentKey, deploymentSequenceNumber, commitId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">get</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/deployments?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposCommitsDeployments(key, environmentKey, deploymentSequenceNumber, commitId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">delete</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/deployments?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postReposCommitsDeployments(commitId, projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">create</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/deployments?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/deployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getstreamPatch(allAncestors, since, until, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">streamPatch</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/patch?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/patch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminUsers(filter, callback)</td>
    <td style="padding:15px">getUsers</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/users?{query} <br /> v1:{base_path}/{version}/admin/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUser(name, callback)</td>
    <td style="padding:15px">deleteUser</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/users?{query} <br /> v1:{base_path}/{version}/admin/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAdminUsers(body, callback)</td>
    <td style="padding:15px">updateUserDetails1</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/users?{query} <br /> v1:{base_path}/{version}/admin/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createUser(name, password, displayName, emailAddress, addToDefaultGroup, notify, callback)</td>
    <td style="padding:15px">createUser</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/users?{query} <br /> v1:{base_path}/{version}/admin/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eraseUser(name, callback)</td>
    <td style="padding:15px">eraseUser</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/users/erasure?{query} <br /> v1:{base_path}/{version}/admin/users/erasure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateErasable(name, callback)</td>
    <td style="padding:15px">validateErasable</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/users/erasure?{query} <br /> v1:{base_path}/{version}/admin/users/erasure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renameUser(body, callback)</td>
    <td style="padding:15px">renameUser</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/users/rename?{query} <br /> v1:{base_path}/{version}/admin/users/rename?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAdminUsersCredentials(body, callback)</td>
    <td style="padding:15px">updateUserPassword1</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/users/credentials?{query} <br /> v1:{base_path}/{version}/admin/users/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearUserCaptchaChallenge(name, callback)</td>
    <td style="padding:15px">clearUserCaptchaChallenge</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/users/captcha?{query} <br /> v1:{base_path}/{version}/admin/users/captcha?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroup(name, callback)</td>
    <td style="padding:15px">deleteGroup</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/groups?{query} <br /> v1:{base_path}/{version}/admin/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroup(name, callback)</td>
    <td style="padding:15px">createGroup</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/groups?{query} <br /> v1:{base_path}/{version}/admin/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminGroups(filter, callback)</td>
    <td style="padding:15px">getGroups</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/groups?{query} <br /> v1:{base_path}/{version}/admin/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserToGroup(body, callback)</td>
    <td style="padding:15px">addUserToGroup</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/groups/add-user?{query} <br /> v1:{base_path}/{version}/admin/groups/add-user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addGroupToUser(body, callback)</td>
    <td style="padding:15px">addGroupToUser</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/users/add-group?{query} <br /> v1:{base_path}/{version}/admin/users/add-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUsersToGroup(body, callback)</td>
    <td style="padding:15px">addUsersToGroup</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/groups/add-users?{query} <br /> v1:{base_path}/{version}/admin/groups/add-users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUserToGroups(body, callback)</td>
    <td style="padding:15px">addUserToGroups</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/users/add-groups?{query} <br /> v1:{base_path}/{version}/admin/users/add-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeUserFromGroup(body, callback)</td>
    <td style="padding:15px">removeUserFromGroup</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/groups/remove-user?{query} <br /> v1:{base_path}/{version}/admin/groups/remove-user?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeGroupFromUser(body, callback)</td>
    <td style="padding:15px">removeGroupFromUser</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/users/remove-group?{query} <br /> v1:{base_path}/{version}/admin/users/remove-group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findUsersInGroup(context, filter, callback)</td>
    <td style="padding:15px">findUsersInGroup</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/groups/more-members?{query} <br /> v1:{base_path}/{version}/admin/groups/more-members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findUsersNotInGroup(context, filter, callback)</td>
    <td style="padding:15px">findUsersNotInGroup</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/groups/more-non-members?{query} <br /> v1:{base_path}/{version}/admin/groups/more-non-members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findGroupsForUser(context, filter, callback)</td>
    <td style="padding:15px">findGroupsForUser</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/users/more-members?{query} <br /> v1:{base_path}/{version}/admin/users/more-members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">findOtherGroupsForUser(context, filter, callback)</td>
    <td style="padding:15px">findOtherGroupsForUser</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/users/more-non-members?{query} <br /> v1:{base_path}/{version}/admin/users/more-non-members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminLicense(callback)</td>
    <td style="padding:15px">get</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/license?{query} <br /> v1:{base_path}/{version}/admin/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postupdate(body, callback)</td>
    <td style="padding:15px">update</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/license?{query} <br /> v1:{base_path}/{version}/admin/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSettingsAutoDecline(projectKey, callback)</td>
    <td style="padding:15px">get</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/auto-decline?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/auto-decline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSettingsAutoDecline(projectKey, callback)</td>
    <td style="padding:15px">delete</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/auto-decline?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/auto-decline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSettingsAutoDecline(projectKey, body, callback)</td>
    <td style="padding:15px">set</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/auto-decline?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/auto-decline?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTask(body, callback)</td>
    <td style="padding:15px">createTask</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/tasks?{query} <br /> v1:{base_path}/{version}/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTask(taskId, callback)</td>
    <td style="padding:15px">deleteTask</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/tasks/{pathv1}?{query} <br /> v1:{base_path}/{version}/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTask(taskId, body, callback)</td>
    <td style="padding:15px">updateTask</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/tasks/{pathv1}?{query} <br /> v1:{base_path}/{version}/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTask(taskId, callback)</td>
    <td style="padding:15px">getTask</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/tasks/{pathv1}?{query} <br /> v1:{base_path}/{version}/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroups(filter, callback)</td>
    <td style="padding:15px">getGroups1</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/groups?{query} <br /> v1:{base_path}/{version}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposPermissionsGroups(filter, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getGroupsWithAnyPermission</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/permissions/groups?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/permissions/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposPermissionsGroups(name, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">revokePermissionsForGroup</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/permissions/groups?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/permissions/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setPermissionForGroup(permission, name, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">setPermissionForGroup</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/permissions/groups?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/permissions/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetGroupsWithoutAnyPermission1(filter, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getGroupsWithoutAnyPermission</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/permissions/groups/none?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/permissions/groups/none?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposPermissionsUsers(filter, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getUsersWithAnyPermission</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/permissions/users?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/permissions/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteReposPermissionsUsers(name, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">revokePermissionsForUser</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/permissions/users?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/permissions/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setPermissionForUser(name, permission, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">setPermissionForUser</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/permissions/users?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/permissions/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetUsersWithoutPermission(filter, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getUsersWithoutPermission</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/permissions/users/none?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/permissions/users/none?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetRepositories(name, projectname, permission, state, visibility, callback)</td>
    <td style="padding:15px">getRepositories</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/repos?{query} <br /> v1:{base_path}/{version}/repos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stream(at, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">stream</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/last-modified?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/last-modified?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getstream(at, projectKey, repositorySlug, uriPath, callback)</td>
    <td style="padding:15px">stream</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/last-modified/{pathv3}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/last-modified/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetCommits(withCounts, avatarSize, avatarScheme, pullRequestId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getCommits</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/commits?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/pull-requests/{pathv3}/commits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationProperties(callback)</td>
    <td style="padding:15px">getApplicationProperties</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/application-properties?{query} <br /> v1:{base_path}/{version}/application-properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetReviewerGroups(projectKey, callback)</td>
    <td style="padding:15px">getReviewerGroups</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/reviewer-groups?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/reviewer-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSettingsReviewerGroups(projectKey, body, callback)</td>
    <td style="padding:15px">create</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/reviewer-groups?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/reviewer-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetReviewerGroup(id, projectKey, callback)</td>
    <td style="padding:15px">getReviewerGroup</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/reviewer-groups/{pathv2}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/reviewer-groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSettingsReviewerGroupsWithid(id, projectKey, body, callback)</td>
    <td style="padding:15px">update</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/reviewer-groups/{pathv2}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/reviewer-groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSettingsReviewerGroupsWithid(id, projectKey, callback)</td>
    <td style="padding:15px">delete</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/settings/reviewer-groups/{pathv2}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/settings/reviewer-groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postcreateComment(since, commitId, projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">createComment</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/comments?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReposCommitsComments(pathParam, since, commitId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getComments</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/comments?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/comments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletedeleteComment(version, commentId, commitId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">deleteComment</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/comments/{pathv4}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putupdateComment(commentId, commitId, projectKey, repositorySlug, body, callback)</td>
    <td style="padding:15px">updateComment</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/comments/{pathv4}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getgetComment(commentId, commitId, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getComment</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/comments/{pathv4}?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/commits/{pathv3}/comments/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setMailConfig(body, callback)</td>
    <td style="padding:15px">setMailConfig</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/mail-server?{query} <br /> v1:{base_path}/{version}/admin/mail-server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMailConfig(callback)</td>
    <td style="padding:15px">getMailConfig</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/mail-server?{query} <br /> v1:{base_path}/{version}/admin/mail-server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMailConfig(callback)</td>
    <td style="padding:15px">deleteMailConfig</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/mail-server?{query} <br /> v1:{base_path}/{version}/admin/mail-server?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSenderAddress(callback)</td>
    <td style="padding:15px">getSenderAddress</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/mail-server/sender-address?{query} <br /> v1:{base_path}/{version}/admin/mail-server/sender-address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearSenderAddress(callback)</td>
    <td style="padding:15px">clearSenderAddress</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/mail-server/sender-address?{query} <br /> v1:{base_path}/{version}/admin/mail-server/sender-address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSenderAddress(body, callback)</td>
    <td style="padding:15px">setSenderAddress</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/admin/mail-server/sender-address?{query} <br /> v1:{base_path}/{version}/admin/mail-server/sender-address?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArchive(at, filename, format, pathParam, prefix, projectKey, repositorySlug, callback)</td>
    <td style="padding:15px">getArchive</td>
    <td style="padding:15px">v0:{base_path}/{version}/api/1.0/projects/{pathv1}/repos/{pathv2}/archive?{query} <br /> v1:{base_path}/{version}/projects/{pathv1}/repos/{pathv2}/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
