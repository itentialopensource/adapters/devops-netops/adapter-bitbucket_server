# Bitbucket Server

Vendor: Atlassian
Homepage: https://www.atlassian.com

Product: Bitbucket Server 
Product Page: https://confluence.atlassian.com/bitbucketserver

## Introduction
We classify Bitbucket Server into the CI/CD domain since Bitbucket Server handles the versioning, building, storage and deployment.

"Bitbucket Server enables you to build, test, and deploy directly from within Bitbucket."

## Why Integrate
The Bitbucket Server adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Bitbucket Server. With this adapter you have the ability to perform operations such as:

- Create, publish, maintain, update repositories in Bitbucket.

## Additional Product Documentation
The [API documents for Bitbucket](https://developer.atlassian.com/server/bitbucket/rest/v815/)