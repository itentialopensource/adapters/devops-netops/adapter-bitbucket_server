/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-bitbucket_server',
      type: 'BitbucketServer',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const BitbucketServer = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Bitbucket_server Adapter Test', () => {
  describe('BitbucketServer Class Tests', () => {
    const a = new BitbucketServer(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('bitbucket_server'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('bitbucket_server'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('BitbucketServer', pronghornDotJson.export);
          assert.equal('Bitbucket_server', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-bitbucket_server', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('bitbucket_server'));
          assert.equal('BitbucketServer', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-bitbucket_server', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-bitbucket_server', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getUsers - errors', () => {
      it('should have a getUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsers - errors', () => {
      it('should have a putUsers function', (done) => {
        try {
          assert.equal(true, typeof a.putUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putUsers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAvatar - errors', () => {
      it('should have a deleteAvatar function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAvatar === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userSlug', (done) => {
        try {
          a.deleteAvatar(null, (data, error) => {
            try {
              const displayE = 'userSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteAvatar', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadAvatar - errors', () => {
      it('should have a uploadAvatar function', (done) => {
        try {
          assert.equal(true, typeof a.uploadAvatar === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userSlug', (done) => {
        try {
          a.uploadAvatar(null, (data, error) => {
            try {
              const displayE = 'userSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-uploadAvatar', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUser - errors', () => {
      it('should have a getUser function', (done) => {
        try {
          assert.equal(true, typeof a.getUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userSlug', (done) => {
        try {
          a.getUser(null, (data, error) => {
            try {
              const displayE = 'userSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putUsersCredentials - errors', () => {
      it('should have a putUsersCredentials function', (done) => {
        try {
          assert.equal(true, typeof a.putUsersCredentials === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putUsersCredentials(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putUsersCredentials', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRefChangeActivity - errors', () => {
      it('should have a getRefChangeActivity function', (done) => {
        try {
          assert.equal(true, typeof a.getRefChangeActivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getRefChangeActivity('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getRefChangeActivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getRefChangeActivity('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getRefChangeActivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findBranches - errors', () => {
      it('should have a findBranches function', (done) => {
        try {
          assert.equal(true, typeof a.findBranches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.findBranches('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-findBranches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.findBranches('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-findBranches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReviewerGroups - errors', () => {
      it('should have a getReviewerGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getReviewerGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReviewerGroups(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReviewerGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReviewerGroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReviewerGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postReposSettingsReviewerGroups - errors', () => {
      it('should have a postReposSettingsReviewerGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postReposSettingsReviewerGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.postReposSettingsReviewerGroups(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposSettingsReviewerGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.postReposSettingsReviewerGroups('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposSettingsReviewerGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postReposSettingsReviewerGroups('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposSettingsReviewerGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetUsers - errors', () => {
      it('should have a getgetUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getgetUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getgetUsers(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetUsers('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getgetUsers('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReviewerGroup - errors', () => {
      it('should have a getReviewerGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getReviewerGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getReviewerGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReviewerGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReviewerGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReviewerGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReviewerGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReviewerGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#update - errors', () => {
      it('should have a update function', (done) => {
        try {
          assert.equal(true, typeof a.update === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.update(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-update', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.update('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-update', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.update('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-update', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.update('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-update', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delete - errors', () => {
      it('should have a delete function', (done) => {
        try {
          assert.equal(true, typeof a.delete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.delete(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-delete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.delete('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-delete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.delete('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-delete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAll - errors', () => {
      it('should have a getAll function', (done) => {
        try {
          assert.equal(true, typeof a.getAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#set - errors', () => {
      it('should have a set function', (done) => {
        try {
          assert.equal(true, typeof a.set === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.set(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-set', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#get - errors', () => {
      it('should have a get function', (done) => {
        try {
          assert.equal(true, typeof a.get === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userSlug', (done) => {
        try {
          a.get(null, (data, error) => {
            try {
              const displayE = 'userSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-get', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAdminRateLimitSettingsUsersWithuserSlug - errors', () => {
      it('should have a deleteAdminRateLimitSettingsUsersWithuserSlug function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAdminRateLimitSettingsUsersWithuserSlug === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userSlug', (done) => {
        try {
          a.deleteAdminRateLimitSettingsUsersWithuserSlug(null, (data, error) => {
            try {
              const displayE = 'userSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteAdminRateLimitSettingsUsersWithuserSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putAdminRateLimitSettingsUsersWithuserSlug - errors', () => {
      it('should have a putAdminRateLimitSettingsUsersWithuserSlug function', (done) => {
        try {
          assert.equal(true, typeof a.putAdminRateLimitSettingsUsersWithuserSlug === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userSlug', (done) => {
        try {
          a.putAdminRateLimitSettingsUsersWithuserSlug(null, null, (data, error) => {
            try {
              const displayE = 'userSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putAdminRateLimitSettingsUsersWithuserSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putAdminRateLimitSettingsUsersWithuserSlug('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putAdminRateLimitSettingsUsersWithuserSlug', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBuildCapabilities - errors', () => {
      it('should have a getBuildCapabilities function', (done) => {
        try {
          assert.equal(true, typeof a.getBuildCapabilities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPullRequestTasks - errors', () => {
      it('should have a getPullRequestTasks function', (done) => {
        try {
          assert.equal(true, typeof a.getPullRequestTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getPullRequestTasks(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getPullRequestTasks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getPullRequestTasks('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getPullRequestTasks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getPullRequestTasks('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getPullRequestTasks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#countPullRequestTasks - errors', () => {
      it('should have a countPullRequestTasks function', (done) => {
        try {
          assert.equal(true, typeof a.countPullRequestTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.countPullRequestTasks(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-countPullRequestTasks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.countPullRequestTasks('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-countPullRequestTasks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.countPullRequestTasks('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-countPullRequestTasks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#streamFiles - errors', () => {
      it('should have a streamFiles function', (done) => {
        try {
          assert.equal(true, typeof a.streamFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.streamFiles('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-streamFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.streamFiles('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-streamFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getstreamFiles - errors', () => {
      it('should have a getstreamFiles function', (done) => {
        try {
          assert.equal(true, typeof a.getstreamFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getstreamFiles('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstreamFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getstreamFiles('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstreamFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing path', (done) => {
        try {
          a.getstreamFiles('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uriPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstreamFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listParticipants - errors', () => {
      it('should have a listParticipants function', (done) => {
        try {
          assert.equal(true, typeof a.listParticipants === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.listParticipants(null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-listParticipants', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.listParticipants('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-listParticipants', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.listParticipants('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-listParticipants', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unassignParticipantRole - errors', () => {
      it('should have a unassignParticipantRole function', (done) => {
        try {
          assert.equal(true, typeof a.unassignParticipantRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.unassignParticipantRole('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-unassignParticipantRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.unassignParticipantRole('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-unassignParticipantRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.unassignParticipantRole('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-unassignParticipantRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignParticipantRole - errors', () => {
      it('should have a assignParticipantRole function', (done) => {
        try {
          assert.equal(true, typeof a.assignParticipantRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.assignParticipantRole(null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-assignParticipantRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.assignParticipantRole('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-assignParticipantRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.assignParticipantRole('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-assignParticipantRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.assignParticipantRole('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-assignParticipantRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateStatus - errors', () => {
      it('should have a updateStatus function', (done) => {
        try {
          assert.equal(true, typeof a.updateStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userSlug', (done) => {
        try {
          a.updateStatus(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'userSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.updateStatus('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.updateStatus('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.updateStatus('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateStatus('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteunassignParticipantRole - errors', () => {
      it('should have a deleteunassignParticipantRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteunassignParticipantRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userSlug', (done) => {
        try {
          a.deleteunassignParticipantRole(null, null, null, null, (data, error) => {
            try {
              const displayE = 'userSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteunassignParticipantRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.deleteunassignParticipantRole('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteunassignParticipantRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteunassignParticipantRole('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteunassignParticipantRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteunassignParticipantRole('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteunassignParticipantRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoriesRecentlyAccessed - errors', () => {
      it('should have a getRepositoriesRecentlyAccessed function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoriesRecentlyAccessed === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#withdrawApproval - errors', () => {
      it('should have a withdrawApproval function', (done) => {
        try {
          assert.equal(true, typeof a.withdrawApproval === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.withdrawApproval(null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-withdrawApproval', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.withdrawApproval('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-withdrawApproval', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.withdrawApproval('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-withdrawApproval', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#approve - errors', () => {
      it('should have a approve function', (done) => {
        try {
          assert.equal(true, typeof a.approve === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.approve(null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-approve', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.approve('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-approve', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.approve('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-approve', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#streamChanges - errors', () => {
      it('should have a streamChanges function', (done) => {
        try {
          assert.equal(true, typeof a.streamChanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.streamChanges('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-streamChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.streamChanges('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-streamChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.streamChanges('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-streamChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCommits - errors', () => {
      it('should have a getCommits function', (done) => {
        try {
          assert.equal(true, typeof a.getCommits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getCommits('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getCommits('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCommit - errors', () => {
      it('should have a getCommit function', (done) => {
        try {
          assert.equal(true, typeof a.getCommit === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.getCommit('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getCommit', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getCommit('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getCommit', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getCommit('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getCommit', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAttachment - errors', () => {
      it('should have a deleteAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentId', (done) => {
        try {
          a.deleteAttachment(null, null, null, (data, error) => {
            try {
              const displayE = 'attachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteAttachment('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteAttachment('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachment - errors', () => {
      it('should have a getAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.getAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentId', (done) => {
        try {
          a.getAttachment(null, null, null, (data, error) => {
            try {
              const displayE = 'attachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getAttachment('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getAttachment('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getAttachment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAttachmentMetadata - errors', () => {
      it('should have a deleteAttachmentMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAttachmentMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentId', (done) => {
        try {
          a.deleteAttachmentMetadata(null, null, null, (data, error) => {
            try {
              const displayE = 'attachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteAttachmentMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteAttachmentMetadata('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteAttachmentMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteAttachmentMetadata('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteAttachmentMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAttachmentMetadata - errors', () => {
      it('should have a getAttachmentMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.getAttachmentMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentId', (done) => {
        try {
          a.getAttachmentMetadata(null, null, null, (data, error) => {
            try {
              const displayE = 'attachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getAttachmentMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getAttachmentMetadata('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getAttachmentMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getAttachmentMetadata('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getAttachmentMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveAttachmentMetadata - errors', () => {
      it('should have a saveAttachmentMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.saveAttachmentMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentId', (done) => {
        try {
          a.saveAttachmentMetadata(null, null, null, null, (data, error) => {
            try {
              const displayE = 'attachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-saveAttachmentMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.saveAttachmentMetadata('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-saveAttachmentMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.saveAttachmentMetadata('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-saveAttachmentMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.saveAttachmentMetadata('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-saveAttachmentMetadata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#streamRawDiff - errors', () => {
      it('should have a streamRawDiff function', (done) => {
        try {
          assert.equal(true, typeof a.streamRawDiff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.streamRawDiff('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-streamRawDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.streamRawDiff('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-streamRawDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposDiffWithpath - errors', () => {
      it('should have a getReposDiffWithpath function', (done) => {
        try {
          assert.equal(true, typeof a.getReposDiffWithpath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposDiffWithpath('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposDiffWithpath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposDiffWithpath('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposDiffWithpath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing path', (done) => {
        try {
          a.getReposDiffWithpath('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uriPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposDiffWithpath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposCommitsPullRequests - errors', () => {
      it('should have a getReposCommitsPullRequests function', (done) => {
        try {
          assert.equal(true, typeof a.getReposCommitsPullRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.getReposCommitsPullRequests(null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsPullRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposCommitsPullRequests('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsPullRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposCommitsPullRequests('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsPullRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getstreamChanges - errors', () => {
      it('should have a getstreamChanges function', (done) => {
        try {
          assert.equal(true, typeof a.getstreamChanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getstreamChanges('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstreamChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getstreamChanges('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstreamChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getstreamDiff1 - errors', () => {
      it('should have a getstreamDiff1 function', (done) => {
        try {
          assert.equal(true, typeof a.getstreamDiff1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getstreamDiff1('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstreamDiff1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getstreamDiff1('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstreamDiff1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing path', (done) => {
        try {
          a.getstreamDiff1('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uriPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstreamDiff1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#streamCommits - errors', () => {
      it('should have a streamCommits function', (done) => {
        try {
          assert.equal(true, typeof a.streamCommits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.streamCommits('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-streamCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.streamCommits('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-streamCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postReposPullRequestsBlockerComments - errors', () => {
      it('should have a postReposPullRequestsBlockerComments function', (done) => {
        try {
          assert.equal(true, typeof a.postReposPullRequestsBlockerComments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.postReposPullRequestsBlockerComments(null, null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposPullRequestsBlockerComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.postReposPullRequestsBlockerComments('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposPullRequestsBlockerComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.postReposPullRequestsBlockerComments('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposPullRequestsBlockerComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postReposPullRequestsBlockerComments('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposPullRequestsBlockerComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposPullRequestsBlockerComments - errors', () => {
      it('should have a getReposPullRequestsBlockerComments function', (done) => {
        try {
          assert.equal(true, typeof a.getReposPullRequestsBlockerComments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposPullRequestsBlockerComments('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsBlockerComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposPullRequestsBlockerComments('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsBlockerComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getReposPullRequestsBlockerComments('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsBlockerComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReposPullRequestsBlockerCommentsWithcommentId - errors', () => {
      it('should have a deleteReposPullRequestsBlockerCommentsWithcommentId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteReposPullRequestsBlockerCommentsWithcommentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.deleteReposPullRequestsBlockerCommentsWithcommentId('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPullRequestsBlockerCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteReposPullRequestsBlockerCommentsWithcommentId('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPullRequestsBlockerCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteReposPullRequestsBlockerCommentsWithcommentId('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPullRequestsBlockerCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.deleteReposPullRequestsBlockerCommentsWithcommentId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPullRequestsBlockerCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putReposPullRequestsBlockerCommentsWithcommentId - errors', () => {
      it('should have a putReposPullRequestsBlockerCommentsWithcommentId function', (done) => {
        try {
          assert.equal(true, typeof a.putReposPullRequestsBlockerCommentsWithcommentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.putReposPullRequestsBlockerCommentsWithcommentId(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposPullRequestsBlockerCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.putReposPullRequestsBlockerCommentsWithcommentId('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposPullRequestsBlockerCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.putReposPullRequestsBlockerCommentsWithcommentId('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposPullRequestsBlockerCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.putReposPullRequestsBlockerCommentsWithcommentId('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposPullRequestsBlockerCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putReposPullRequestsBlockerCommentsWithcommentId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposPullRequestsBlockerCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposPullRequestsBlockerCommentsWithcommentId - errors', () => {
      it('should have a getReposPullRequestsBlockerCommentsWithcommentId function', (done) => {
        try {
          assert.equal(true, typeof a.getReposPullRequestsBlockerCommentsWithcommentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.getReposPullRequestsBlockerCommentsWithcommentId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsBlockerCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposPullRequestsBlockerCommentsWithcommentId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsBlockerCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposPullRequestsBlockerCommentsWithcommentId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsBlockerCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getReposPullRequestsBlockerCommentsWithcommentId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsBlockerCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposSettingsAutoDecline - errors', () => {
      it('should have a getReposSettingsAutoDecline function', (done) => {
        try {
          assert.equal(true, typeof a.getReposSettingsAutoDecline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposSettingsAutoDecline(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposSettingsAutoDecline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposSettingsAutoDecline('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposSettingsAutoDecline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReposSettingsAutoDecline - errors', () => {
      it('should have a deleteReposSettingsAutoDecline function', (done) => {
        try {
          assert.equal(true, typeof a.deleteReposSettingsAutoDecline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteReposSettingsAutoDecline(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposSettingsAutoDecline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteReposSettingsAutoDecline('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposSettingsAutoDecline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putReposSettingsAutoDecline - errors', () => {
      it('should have a putReposSettingsAutoDecline function', (done) => {
        try {
          assert.equal(true, typeof a.putReposSettingsAutoDecline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.putReposSettingsAutoDecline(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposSettingsAutoDecline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.putReposSettingsAutoDecline('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposSettingsAutoDecline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putReposSettingsAutoDecline('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposSettingsAutoDecline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInformation - errors', () => {
      it('should have a getInformation function', (done) => {
        try {
          assert.equal(true, typeof a.getInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoryHooks - errors', () => {
      it('should have a getRepositoryHooks function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoryHooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getRepositoryHooks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getRepositoryHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setSettings - errors', () => {
      it('should have a setSettings function', (done) => {
        try {
          assert.equal(true, typeof a.setSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookKey', (done) => {
        try {
          a.setSettings(null, null, null, (data, error) => {
            try {
              const displayE = 'hookKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.setSettings('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setSettings('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSettingsHooksSettings - errors', () => {
      it('should have a getSettingsHooksSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getSettingsHooksSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookKey', (done) => {
        try {
          a.getSettingsHooksSettings(null, null, (data, error) => {
            try {
              const displayE = 'hookKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getSettingsHooksSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getSettingsHooksSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getSettingsHooksSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositoryHook - errors', () => {
      it('should have a getRepositoryHook function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositoryHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookKey', (done) => {
        try {
          a.getRepositoryHook(null, null, (data, error) => {
            try {
              const displayE = 'hookKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getRepositoryHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getRepositoryHook('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getRepositoryHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableHook - errors', () => {
      it('should have a enableHook function', (done) => {
        try {
          assert.equal(true, typeof a.enableHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookKey', (done) => {
        try {
          a.enableHook(null, null, (data, error) => {
            try {
              const displayE = 'hookKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-enableHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.enableHook('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-enableHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableHook - errors', () => {
      it('should have a disableHook function', (done) => {
        try {
          assert.equal(true, typeof a.disableHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookKey', (done) => {
        try {
          a.disableHook(null, null, (data, error) => {
            try {
              const displayE = 'hookKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-disableHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.disableHook('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-disableHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdminPermissionsGroups - errors', () => {
      it('should have a getAdminPermissionsGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getAdminPermissionsGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setPermissionForGroups - errors', () => {
      it('should have a setPermissionForGroups function', (done) => {
        try {
          assert.equal(true, typeof a.setPermissionForGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokePermissionsForGroup - errors', () => {
      it('should have a revokePermissionsForGroup function', (done) => {
        try {
          assert.equal(true, typeof a.revokePermissionsForGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdminPermissionsGroupsNone - errors', () => {
      it('should have a getAdminPermissionsGroupsNone function', (done) => {
        try {
          assert.equal(true, typeof a.getAdminPermissionsGroupsNone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdminPermissionsUsers - errors', () => {
      it('should have a getAdminPermissionsUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getAdminPermissionsUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setPermissionForUsers - errors', () => {
      it('should have a setPermissionForUsers function', (done) => {
        try {
          assert.equal(true, typeof a.setPermissionForUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokePermissionsForUser - errors', () => {
      it('should have a revokePermissionsForUser function', (done) => {
        try {
          assert.equal(true, typeof a.revokePermissionsForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersWithoutAnyPermission - errors', () => {
      it('should have a getUsersWithoutAnyPermission function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersWithoutAnyPermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMergeConfig - errors', () => {
      it('should have a getMergeConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getMergeConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scmId', (done) => {
        try {
          a.getMergeConfig(null, (data, error) => {
            try {
              const displayE = 'scmId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getMergeConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setMergeConfig - errors', () => {
      it('should have a setMergeConfig function', (done) => {
        try {
          assert.equal(true, typeof a.setMergeConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scmId', (done) => {
        try {
          a.setMergeConfig(null, null, (data, error) => {
            try {
              const displayE = 'scmId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setMergeConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setMergeConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setMergeConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPage - errors', () => {
      it('should have a getPage function', (done) => {
        try {
          assert.equal(true, typeof a.getPage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getPage('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getPage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getPage('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getPage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postReposPullRequests - errors', () => {
      it('should have a postReposPullRequests function', (done) => {
        try {
          assert.equal(true, typeof a.postReposPullRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.postReposPullRequests(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposPullRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.postReposPullRequests('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposPullRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postReposPullRequests('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposPullRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposPullRequestsWithpullRequestIdDiff - errors', () => {
      it('should have a getReposPullRequestsWithpullRequestIdDiff function', (done) => {
        try {
          assert.equal(true, typeof a.getReposPullRequestsWithpullRequestIdDiff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposPullRequestsWithpullRequestIdDiff('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsWithpullRequestIdDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposPullRequestsWithpullRequestIdDiff('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsWithpullRequestIdDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getReposPullRequestsWithpullRequestIdDiff('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsWithpullRequestIdDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reopen - errors', () => {
      it('should have a reopen function', (done) => {
        try {
          assert.equal(true, typeof a.reopen === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.reopen('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-reopen', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.reopen('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-reopen', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.reopen('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-reopen', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#streamPatch - errors', () => {
      it('should have a streamPatch function', (done) => {
        try {
          assert.equal(true, typeof a.streamPatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.streamPatch(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-streamPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.streamPatch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-streamPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.streamPatch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-streamPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActivities - errors', () => {
      it('should have a getActivities function', (done) => {
        try {
          assert.equal(true, typeof a.getActivities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getActivities('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getActivities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getActivities('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getActivities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getActivities('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getActivities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#canMerge - errors', () => {
      it('should have a canMerge function', (done) => {
        try {
          assert.equal(true, typeof a.canMerge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.canMerge(null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-canMerge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.canMerge('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-canMerge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.canMerge('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-canMerge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#merge - errors', () => {
      it('should have a merge function', (done) => {
        try {
          assert.equal(true, typeof a.merge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.merge('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-merge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.merge('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-merge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.merge('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-merge', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#decline - errors', () => {
      it('should have a decline function', (done) => {
        try {
          assert.equal(true, typeof a.decline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.decline('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-decline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.decline('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-decline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.decline('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-decline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposPullRequestsWithpullRequestId - errors', () => {
      it('should have a getReposPullRequestsWithpullRequestId function', (done) => {
        try {
          assert.equal(true, typeof a.getReposPullRequestsWithpullRequestId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getReposPullRequestsWithpullRequestId(null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsWithpullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposPullRequestsWithpullRequestId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsWithpullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposPullRequestsWithpullRequestId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsWithpullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putReposPullRequestsWithpullRequestId - errors', () => {
      it('should have a putReposPullRequestsWithpullRequestId function', (done) => {
        try {
          assert.equal(true, typeof a.putReposPullRequestsWithpullRequestId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.putReposPullRequestsWithpullRequestId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposPullRequestsWithpullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.putReposPullRequestsWithpullRequestId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposPullRequestsWithpullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.putReposPullRequestsWithpullRequestId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposPullRequestsWithpullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putReposPullRequestsWithpullRequestId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposPullRequestsWithpullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReposPullRequestsWithpullRequestId - errors', () => {
      it('should have a deleteReposPullRequestsWithpullRequestId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteReposPullRequestsWithpullRequestId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.deleteReposPullRequestsWithpullRequestId(null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPullRequestsWithpullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteReposPullRequestsWithpullRequestId('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPullRequestsWithpullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteReposPullRequestsWithpullRequestId('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPullRequestsWithpullRequestId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBanner - errors', () => {
      it('should have a getBanner function', (done) => {
        try {
          assert.equal(true, typeof a.getBanner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteBanner - errors', () => {
      it('should have a deleteBanner function', (done) => {
        try {
          assert.equal(true, typeof a.deleteBanner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setBanner - errors', () => {
      it('should have a setBanner function', (done) => {
        try {
          assert.equal(true, typeof a.setBanner === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setBanner(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setBanner', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unwatch - errors', () => {
      it('should have a unwatch function', (done) => {
        try {
          assert.equal(true, typeof a.unwatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.unwatch(null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-unwatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.unwatch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-unwatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.unwatch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-unwatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#watch - errors', () => {
      it('should have a watch function', (done) => {
        try {
          assert.equal(true, typeof a.watch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.watch(null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-watch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.watch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-watch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.watch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-watch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putsetSettings - errors', () => {
      it('should have a putsetSettings function', (done) => {
        try {
          assert.equal(true, typeof a.putsetSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putsetSettings(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putsetSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetSettings - errors', () => {
      it('should have a getgetSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getgetSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChanges - errors', () => {
      it('should have a getChanges function', (done) => {
        try {
          assert.equal(true, typeof a.getChanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.getChanges('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getChanges('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getChanges('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdminDefaultBranch - errors', () => {
      it('should have a getAdminDefaultBranch function', (done) => {
        try {
          assert.equal(true, typeof a.getAdminDefaultBranch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setDefaultBranch - errors', () => {
      it('should have a setDefaultBranch function', (done) => {
        try {
          assert.equal(true, typeof a.setDefaultBranch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setDefaultBranch(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setDefaultBranch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearDefaultBranch - errors', () => {
      it('should have a clearDefaultBranch function', (done) => {
        try {
          assert.equal(true, typeof a.clearDefaultBranch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposPullRequestsDiff - errors', () => {
      it('should have a getReposPullRequestsDiff function', (done) => {
        try {
          assert.equal(true, typeof a.getReposPullRequestsDiff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposPullRequestsDiff('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposPullRequestsDiff('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getReposPullRequestsDiff('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposPullRequestsDiffWithpath - errors', () => {
      it('should have a getReposPullRequestsDiffWithpath function', (done) => {
        try {
          assert.equal(true, typeof a.getReposPullRequestsDiffWithpath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposPullRequestsDiffWithpath('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsDiffWithpath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposPullRequestsDiffWithpath('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsDiffWithpath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getReposPullRequestsDiffWithpath('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsDiffWithpath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing path', (done) => {
        try {
          a.getReposPullRequestsDiffWithpath('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uriPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsDiffWithpath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#streamRaw - errors', () => {
      it('should have a streamRaw function', (done) => {
        try {
          assert.equal(true, typeof a.streamRaw === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.streamRaw(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-streamRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.streamRaw('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-streamRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getstreamRaw - errors', () => {
      it('should have a getstreamRaw function', (done) => {
        try {
          assert.equal(true, typeof a.getstreamRaw === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getstreamRaw('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstreamRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getstreamRaw('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstreamRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing path', (done) => {
        try {
          a.getstreamRaw('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uriPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstreamRaw', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#add - errors', () => {
      it('should have a add function', (done) => {
        try {
          assert.equal(true, typeof a.add === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.add(null, null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-add', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.add('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-add', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.add('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-add', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.add('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-add', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposCommitsBuilds - errors', () => {
      it('should have a getReposCommitsBuilds function', (done) => {
        try {
          assert.equal(true, typeof a.getReposCommitsBuilds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.getReposCommitsBuilds('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsBuilds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposCommitsBuilds('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsBuilds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposCommitsBuilds('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsBuilds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReposCommitsBuilds - errors', () => {
      it('should have a deleteReposCommitsBuilds function', (done) => {
        try {
          assert.equal(true, typeof a.deleteReposCommitsBuilds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.deleteReposCommitsBuilds('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposCommitsBuilds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteReposCommitsBuilds('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposCommitsBuilds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteReposCommitsBuilds('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposCommitsBuilds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRepository - errors', () => {
      it('should have a createRepository function', (done) => {
        try {
          assert.equal(true, typeof a.createRepository === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.createRepository(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-createRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRepository('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-createRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepositories - errors', () => {
      it('should have a getRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.getRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getRepositories(null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetDefaultBranch - errors', () => {
      it('should have a getgetDefaultBranch function', (done) => {
        try {
          assert.equal(true, typeof a.getgetDefaultBranch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetDefaultBranch(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetDefaultBranch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getgetDefaultBranch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetDefaultBranch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putsetDefaultBranch - errors', () => {
      it('should have a putsetDefaultBranch function', (done) => {
        try {
          assert.equal(true, typeof a.putsetDefaultBranch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.putsetDefaultBranch(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putsetDefaultBranch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.putsetDefaultBranch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putsetDefaultBranch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putsetDefaultBranch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putsetDefaultBranch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepository - errors', () => {
      it('should have a deleteRepository function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepository === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteRepository(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteRepository('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#forkRepository - errors', () => {
      it('should have a forkRepository function', (done) => {
        try {
          assert.equal(true, typeof a.forkRepository === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.forkRepository(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-forkRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.forkRepository('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-forkRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.forkRepository('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-forkRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRepository - errors', () => {
      it('should have a getRepository function', (done) => {
        try {
          assert.equal(true, typeof a.getRepository === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getRepository(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getRepository('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateRepository - errors', () => {
      it('should have a updateRepository function', (done) => {
        try {
          assert.equal(true, typeof a.updateRepository === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.updateRepository(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.updateRepository('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateRepository('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findContributing - errors', () => {
      it('should have a findContributing function', (done) => {
        try {
          assert.equal(true, typeof a.findContributing === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.findContributing(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-findContributing', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.findContributing('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-findContributing', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findLicense - errors', () => {
      it('should have a findLicense function', (done) => {
        try {
          assert.equal(true, typeof a.findLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.findLicense(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-findLicense', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.findLicense('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-findLicense', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findReadme - errors', () => {
      it('should have a findReadme function', (done) => {
        try {
          assert.equal(true, typeof a.findReadme === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.findReadme(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-findReadme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.findReadme('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-findReadme', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getForkedRepositories - errors', () => {
      it('should have a getForkedRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.getForkedRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getForkedRepositories(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getForkedRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getForkedRepositories('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getForkedRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRelatedRepositories - errors', () => {
      it('should have a getRelatedRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.getRelatedRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getRelatedRepositories(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getRelatedRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getRelatedRepositories('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getRelatedRepositories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retryCreateRepository - errors', () => {
      it('should have a retryCreateRepository function', (done) => {
        try {
          assert.equal(true, typeof a.retryCreateRepository === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.retryCreateRepository(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-retryCreateRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.retryCreateRepository('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-retryCreateRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReposWatch - errors', () => {
      it('should have a deleteReposWatch function', (done) => {
        try {
          assert.equal(true, typeof a.deleteReposWatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteReposWatch(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteReposWatch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postReposWatch - errors', () => {
      it('should have a postReposWatch function', (done) => {
        try {
          assert.equal(true, typeof a.postReposWatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.postReposWatch(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.postReposWatch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvatar - errors', () => {
      it('should have a getAvatar function', (done) => {
        try {
          assert.equal(true, typeof a.getAvatar === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookKey', (done) => {
        try {
          a.getAvatar('fakeparam', null, (data, error) => {
            try {
              const displayE = 'hookKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getAvatar', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#search - errors', () => {
      it('should have a search function', (done) => {
        try {
          assert.equal(true, typeof a.search === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.search('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-search', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.search('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-search', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReposPullRequestsWatch - errors', () => {
      it('should have a deleteReposPullRequestsWatch function', (done) => {
        try {
          assert.equal(true, typeof a.deleteReposPullRequestsWatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteReposPullRequestsWatch(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPullRequestsWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteReposPullRequestsWatch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPullRequestsWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.deleteReposPullRequestsWatch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPullRequestsWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postReposPullRequestsWatch - errors', () => {
      it('should have a postReposPullRequestsWatch function', (done) => {
        try {
          assert.equal(true, typeof a.postReposPullRequestsWatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.postReposPullRequestsWatch(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposPullRequestsWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.postReposPullRequestsWatch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposPullRequestsWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.postReposPullRequestsWatch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposPullRequestsWatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContent - errors', () => {
      it('should have a getContent function', (done) => {
        try {
          assert.equal(true, typeof a.getContent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getContent('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getContent('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getContent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editFile - errors', () => {
      it('should have a editFile function', (done) => {
        try {
          assert.equal(true, typeof a.editFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.editFile(null, null, null, '', '', '', '', '', (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-editFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.editFile('fakeparam', null, null, '', '', '', '', '', (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-editFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uriPath', (done) => {
        try {
          a.editFile('fakeparam', 'fakeparam', null, '', '', '', '', '', (data, error) => {
            try {
              const displayE = 'uriPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-editFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editFiles - errors', () => {
      it('should have a editFiles function', (done) => {
        try {
          assert.equal(true, typeof a.editFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetContent - errors', () => {
      it('should have a getgetContent function', (done) => {
        try {
          assert.equal(true, typeof a.getgetContent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetContent('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetContentWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getgetContent('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetContentWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing path', (done) => {
        try {
          a.getgetContent('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uriPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetContentWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetContentWithOptions - errors', () => {
      it('should have a getgetContent function', (done) => {
        try {
          assert.equal(true, typeof a.getgetContentWithOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetContentWithOptions(null, null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetContentWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getgetContentWithOptions(null, 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetContentWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing path', (done) => {
        try {
          a.getgetContentWithOptions(null, 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uriPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetContentWithOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPullRequestSuggestions - errors', () => {
      it('should have a getPullRequestSuggestions function', (done) => {
        try {
          assert.equal(true, typeof a.getPullRequestSuggestions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetPullRequests - errors', () => {
      it('should have a getgetPullRequests function', (done) => {
        try {
          assert.equal(true, typeof a.getgetPullRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPullRequestCount - errors', () => {
      it('should have a getPullRequestCount function', (done) => {
        try {
          assert.equal(true, typeof a.getPullRequestCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInboxPullRequests - errors', () => {
      it('should have a getInboxPullRequests function', (done) => {
        try {
          assert.equal(true, typeof a.getInboxPullRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postReposPullRequestsComments - errors', () => {
      it('should have a postReposPullRequestsComments function', (done) => {
        try {
          assert.equal(true, typeof a.postReposPullRequestsComments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.postReposPullRequestsComments(null, null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposPullRequestsComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.postReposPullRequestsComments('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposPullRequestsComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.postReposPullRequestsComments('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposPullRequestsComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postReposPullRequestsComments('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposPullRequestsComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetComments - errors', () => {
      it('should have a getgetComments function', (done) => {
        try {
          assert.equal(true, typeof a.getgetComments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetComments('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getgetComments('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getgetComments('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReposPullRequestsCommentsWithcommentId - errors', () => {
      it('should have a deleteReposPullRequestsCommentsWithcommentId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteReposPullRequestsCommentsWithcommentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.deleteReposPullRequestsCommentsWithcommentId('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPullRequestsCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteReposPullRequestsCommentsWithcommentId('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPullRequestsCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteReposPullRequestsCommentsWithcommentId('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPullRequestsCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.deleteReposPullRequestsCommentsWithcommentId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPullRequestsCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putReposPullRequestsCommentsWithcommentId - errors', () => {
      it('should have a putReposPullRequestsCommentsWithcommentId function', (done) => {
        try {
          assert.equal(true, typeof a.putReposPullRequestsCommentsWithcommentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.putReposPullRequestsCommentsWithcommentId(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposPullRequestsCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.putReposPullRequestsCommentsWithcommentId('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposPullRequestsCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.putReposPullRequestsCommentsWithcommentId('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposPullRequestsCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.putReposPullRequestsCommentsWithcommentId('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposPullRequestsCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putReposPullRequestsCommentsWithcommentId('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putReposPullRequestsCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposPullRequestsCommentsWithcommentId - errors', () => {
      it('should have a getReposPullRequestsCommentsWithcommentId function', (done) => {
        try {
          assert.equal(true, typeof a.getReposPullRequestsCommentsWithcommentId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.getReposPullRequestsCommentsWithcommentId(null, null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposPullRequestsCommentsWithcommentId('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposPullRequestsCommentsWithcommentId('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getReposPullRequestsCommentsWithcommentId('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPullRequestsCommentsWithcommentId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applySuggestion - errors', () => {
      it('should have a applySuggestion function', (done) => {
        try {
          assert.equal(true, typeof a.applySuggestion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.applySuggestion(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-applySuggestion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.applySuggestion('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-applySuggestion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.applySuggestion('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-applySuggestion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.applySuggestion('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-applySuggestion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.applySuggestion('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-applySuggestion', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findWebhooks - errors', () => {
      it('should have a findWebhooks function', (done) => {
        try {
          assert.equal(true, typeof a.findWebhooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.findWebhooks('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-findWebhooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.findWebhooks('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-findWebhooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createWebhook - errors', () => {
      it('should have a createWebhook function', (done) => {
        try {
          assert.equal(true, typeof a.createWebhook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.createWebhook(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-createWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.createWebhook('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-createWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createWebhook('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-createWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebhook - errors', () => {
      it('should have a deleteWebhook function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebhook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing webhookId', (done) => {
        try {
          a.deleteWebhook(null, null, null, (data, error) => {
            try {
              const displayE = 'webhookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteWebhook('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteWebhook('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebhook - errors', () => {
      it('should have a getWebhook function', (done) => {
        try {
          assert.equal(true, typeof a.getWebhook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing webhookId', (done) => {
        try {
          a.getWebhook('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'webhookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getWebhook('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getWebhook('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateWebhook - errors', () => {
      it('should have a updateWebhook function', (done) => {
        try {
          assert.equal(true, typeof a.updateWebhook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing webhookId', (done) => {
        try {
          a.updateWebhook(null, null, null, null, (data, error) => {
            try {
              const displayE = 'webhookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.updateWebhook('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.updateWebhook('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateWebhook('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLatestInvocation - errors', () => {
      it('should have a getLatestInvocation function', (done) => {
        try {
          assert.equal(true, typeof a.getLatestInvocation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing webhookId', (done) => {
        try {
          a.getLatestInvocation('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'webhookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getLatestInvocation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getLatestInvocation('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getLatestInvocation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getLatestInvocation('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getLatestInvocation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatistics - errors', () => {
      it('should have a getStatistics function', (done) => {
        try {
          assert.equal(true, typeof a.getStatistics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing webhookId', (done) => {
        try {
          a.getStatistics('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'webhookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getStatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getStatistics('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getStatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getStatistics('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getStatistics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatisticsSummary - errors', () => {
      it('should have a getStatisticsSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getStatisticsSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing webhookId', (done) => {
        try {
          a.getStatisticsSummary(null, null, null, (data, error) => {
            try {
              const displayE = 'webhookId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getStatisticsSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getStatisticsSummary('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getStatisticsSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getStatisticsSummary('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getStatisticsSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#testWebhook - errors', () => {
      it('should have a testWebhook function', (done) => {
        try {
          assert.equal(true, typeof a.testWebhook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.testWebhook('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-testWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.testWebhook('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-testWebhook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetChanges - errors', () => {
      it('should have a getgetChanges function', (done) => {
        try {
          assert.equal(true, typeof a.getgetChanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetChanges('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getgetChanges('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBranches - errors', () => {
      it('should have a getBranches function', (done) => {
        try {
          assert.equal(true, typeof a.getBranches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getBranches('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getBranches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getBranches('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getBranches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createBranch - errors', () => {
      it('should have a createBranch function', (done) => {
        try {
          assert.equal(true, typeof a.createBranch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.createBranch(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-createBranch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.createBranch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-createBranch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createBranch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-createBranch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposBranchesDefault - errors', () => {
      it('should have a getReposBranchesDefault function', (done) => {
        try {
          assert.equal(true, typeof a.getReposBranchesDefault === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposBranchesDefault(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposBranchesDefault', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposBranchesDefault('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposBranchesDefault', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setDefaultBranch1 - errors', () => {
      it('should have a setDefaultBranch1 function', (done) => {
        try {
          assert.equal(true, typeof a.setDefaultBranch1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.setDefaultBranch1(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setDefaultBranch1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.setDefaultBranch1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setDefaultBranch1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setDefaultBranch1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setDefaultBranch1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelImportJob - errors', () => {
      it('should have a cancelImportJob function', (done) => {
        try {
          assert.equal(true, typeof a.cancelImportJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.cancelImportJob(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-cancelImportJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExportJob - errors', () => {
      it('should have a getExportJob function', (done) => {
        try {
          assert.equal(true, typeof a.getExportJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getExportJob(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getExportJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelExportJob - errors', () => {
      it('should have a cancelExportJob function', (done) => {
        try {
          assert.equal(true, typeof a.cancelExportJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.cancelExportJob(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-cancelExportJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExportJobMessages - errors', () => {
      it('should have a getExportJobMessages function', (done) => {
        try {
          assert.equal(true, typeof a.getExportJobMessages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getExportJobMessages('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getExportJobMessages', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImportJobMessages - errors', () => {
      it('should have a getImportJobMessages function', (done) => {
        try {
          assert.equal(true, typeof a.getImportJobMessages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getImportJobMessages('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getImportJobMessages', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#previewExport - errors', () => {
      it('should have a previewExport function', (done) => {
        try {
          assert.equal(true, typeof a.previewExport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.previewExport(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-previewExport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startExport - errors', () => {
      it('should have a startExport function', (done) => {
        try {
          assert.equal(true, typeof a.startExport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.startExport(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-startExport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startImport - errors', () => {
      it('should have a startImport function', (done) => {
        try {
          assert.equal(true, typeof a.startImport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.startImport(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-startImport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImportJob - errors', () => {
      it('should have a getImportJob function', (done) => {
        try {
          assert.equal(true, typeof a.getImportJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.getImportJob(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getImportJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposCommitsDiff - errors', () => {
      it('should have a getReposCommitsDiff function', (done) => {
        try {
          assert.equal(true, typeof a.getReposCommitsDiff === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.getReposCommitsDiff('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposCommitsDiff('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposCommitsDiff('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsDiff', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposCommitsDiffWithpath - errors', () => {
      it('should have a getReposCommitsDiffWithpath function', (done) => {
        try {
          assert.equal(true, typeof a.getReposCommitsDiffWithpath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.getReposCommitsDiffWithpath('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsDiffWithpath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposCommitsDiffWithpath('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsDiffWithpath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposCommitsDiffWithpath('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsDiffWithpath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing path', (done) => {
        try {
          a.getReposCommitsDiffWithpath('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uriPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsDiffWithpath', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setRootLevel - errors', () => {
      it('should have a setRootLevel function', (done) => {
        try {
          assert.equal(true, typeof a.setRootLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing levelName', (done) => {
        try {
          a.setRootLevel(null, (data, error) => {
            try {
              const displayE = 'levelName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setRootLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRootLevel - errors', () => {
      it('should have a getRootLevel function', (done) => {
        try {
          assert.equal(true, typeof a.getRootLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setLevel - errors', () => {
      it('should have a setLevel function', (done) => {
        try {
          assert.equal(true, typeof a.setLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing levelName', (done) => {
        try {
          a.setLevel(null, null, (data, error) => {
            try {
              const displayE = 'levelName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loggerName', (done) => {
        try {
          a.setLevel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'loggerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLevel - errors', () => {
      it('should have a getLevel function', (done) => {
        try {
          assert.equal(true, typeof a.getLevel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loggerName', (done) => {
        try {
          a.getLevel(null, (data, error) => {
            try {
              const displayE = 'loggerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getLevel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLabels - errors', () => {
      it('should have a getLabels function', (done) => {
        try {
          assert.equal(true, typeof a.getLabels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLabelsLabeled - errors', () => {
      it('should have a getLabelsLabeled function', (done) => {
        try {
          assert.equal(true, typeof a.getLabelsLabeled === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labelName', (done) => {
        try {
          a.getLabelsLabeled('fakeparam', null, (data, error) => {
            try {
              const displayE = 'labelName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getLabelsLabeled', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLabelsWithlabelName - errors', () => {
      it('should have a getLabelsWithlabelName function', (done) => {
        try {
          assert.equal(true, typeof a.getLabelsWithlabelName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labelName', (done) => {
        try {
          a.getLabelsWithlabelName(null, (data, error) => {
            try {
              const displayE = 'labelName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getLabelsWithlabelName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHistory - errors', () => {
      it('should have a getHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createProject - errors', () => {
      it('should have a createProject function', (done) => {
        try {
          assert.equal(true, typeof a.createProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createProject(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-createProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProjects - errors', () => {
      it('should have a getProjects function', (done) => {
        try {
          assert.equal(true, typeof a.getProjects === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProjectAvatar - errors', () => {
      it('should have a getProjectAvatar function', (done) => {
        try {
          assert.equal(true, typeof a.getProjectAvatar === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getProjectAvatar('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getProjectAvatar', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postuploadAvatar - errors', () => {
      it('should have a postuploadAvatar function', (done) => {
        try {
          assert.equal(true, typeof a.postuploadAvatar === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.postuploadAvatar(null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postuploadAvatar', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteProject - errors', () => {
      it('should have a deleteProject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteProject(null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateProject - errors', () => {
      it('should have a updateProject function', (done) => {
        try {
          assert.equal(true, typeof a.updateProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.updateProject(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateProject('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProject - errors', () => {
      it('should have a getProject function', (done) => {
        try {
          assert.equal(true, typeof a.getProject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getProject(null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getProject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetGroupsWithAnyPermission - errors', () => {
      it('should have a getgetGroupsWithAnyPermission function', (done) => {
        try {
          assert.equal(true, typeof a.getgetGroupsWithAnyPermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetGroupsWithAnyPermission('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetGroupsWithAnyPermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putsetPermissionForGroups - errors', () => {
      it('should have a putsetPermissionForGroups function', (done) => {
        try {
          assert.equal(true, typeof a.putsetPermissionForGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.putsetPermissionForGroups('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putsetPermissionForGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsGroups - errors', () => {
      it('should have a deletePermissionsGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionsGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deletePermissionsGroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deletePermissionsGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetGroupsWithoutAnyPermission - errors', () => {
      it('should have a getgetGroupsWithoutAnyPermission function', (done) => {
        try {
          assert.equal(true, typeof a.getgetGroupsWithoutAnyPermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetGroupsWithoutAnyPermission('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetGroupsWithoutAnyPermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetUsersWithAnyPermission - errors', () => {
      it('should have a getgetUsersWithAnyPermission function', (done) => {
        try {
          assert.equal(true, typeof a.getgetUsersWithAnyPermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetUsersWithAnyPermission('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetUsersWithAnyPermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putsetPermissionForUsers - errors', () => {
      it('should have a putsetPermissionForUsers function', (done) => {
        try {
          assert.equal(true, typeof a.putsetPermissionForUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.putsetPermissionForUsers('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putsetPermissionForUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePermissionsUsers - errors', () => {
      it('should have a deletePermissionsUsers function', (done) => {
        try {
          assert.equal(true, typeof a.deletePermissionsUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deletePermissionsUsers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deletePermissionsUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersWithoutPermission - errors', () => {
      it('should have a getUsersWithoutPermission function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersWithoutPermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getUsersWithoutPermission('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getUsersWithoutPermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hasAllUserPermission - errors', () => {
      it('should have a hasAllUserPermission function', (done) => {
        try {
          assert.equal(true, typeof a.hasAllUserPermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing permission', (done) => {
        try {
          a.hasAllUserPermission(null, null, (data, error) => {
            try {
              const displayE = 'permission is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-hasAllUserPermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.hasAllUserPermission('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-hasAllUserPermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyAllUserPermission - errors', () => {
      it('should have a modifyAllUserPermission function', (done) => {
        try {
          assert.equal(true, typeof a.modifyAllUserPermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing permission', (done) => {
        try {
          a.modifyAllUserPermission('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'permission is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-modifyAllUserPermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.modifyAllUserPermission('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-modifyAllUserPermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPullRequestSettings - errors', () => {
      it('should have a getPullRequestSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getPullRequestSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getPullRequestSettings(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getPullRequestSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getPullRequestSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getPullRequestSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePullRequestSettings - errors', () => {
      it('should have a updatePullRequestSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updatePullRequestSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.updatePullRequestSettings(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updatePullRequestSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.updatePullRequestSettings('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updatePullRequestSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePullRequestSettings('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updatePullRequestSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSettings - errors', () => {
      it('should have a updateSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userSlug', (done) => {
        try {
          a.updateSettings(null, null, (data, error) => {
            try {
              const displayE = 'userSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserSettings - errors', () => {
      it('should have a getUserSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getUserSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userSlug', (done) => {
        try {
          a.getUserSettings(null, (data, error) => {
            try {
              const displayE = 'userSlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getUserSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReview - errors', () => {
      it('should have a getReview function', (done) => {
        try {
          assert.equal(true, typeof a.getReview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReview(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReview('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getReview('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discardReview - errors', () => {
      it('should have a discardReview function', (done) => {
        try {
          assert.equal(true, typeof a.discardReview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.discardReview(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-discardReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.discardReview('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-discardReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.discardReview('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-discardReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#finishReview - errors', () => {
      it('should have a finishReview function', (done) => {
        try {
          assert.equal(true, typeof a.finishReview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.finishReview(null, null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-finishReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.finishReview('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-finishReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.finishReview('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-finishReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.finishReview('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-finishReview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#preview - errors', () => {
      it('should have a preview function', (done) => {
        try {
          assert.equal(true, typeof a.preview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.preview('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-preview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetRepositoryHooks - errors', () => {
      it('should have a getgetRepositoryHooks function', (done) => {
        try {
          assert.equal(true, typeof a.getgetRepositoryHooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetRepositoryHooks('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetRepositoryHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getgetRepositoryHooks('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetRepositoryHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putsetSettings1 - errors', () => {
      it('should have a putsetSettings1 function', (done) => {
        try {
          assert.equal(true, typeof a.putsetSettings1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookKey', (done) => {
        try {
          a.putsetSettings1(null, null, null, null, (data, error) => {
            try {
              const displayE = 'hookKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putsetSettings1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.putsetSettings1('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putsetSettings1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.putsetSettings1('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putsetSettings1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putsetSettings1('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putsetSettings1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposHooks - errors', () => {
      it('should have a getReposHooks function', (done) => {
        try {
          assert.equal(true, typeof a.getReposHooks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookKey', (done) => {
        try {
          a.getReposHooks(null, null, null, (data, error) => {
            try {
              const displayE = 'hookKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposHooks('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposHooks('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposHooks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetRepositoryHook - errors', () => {
      it('should have a getgetRepositoryHook function', (done) => {
        try {
          assert.equal(true, typeof a.getgetRepositoryHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookKey', (done) => {
        try {
          a.getgetRepositoryHook(null, null, null, (data, error) => {
            try {
              const displayE = 'hookKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetRepositoryHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetRepositoryHook('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetRepositoryHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getgetRepositoryHook('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetRepositoryHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRepositoryHook - errors', () => {
      it('should have a deleteRepositoryHook function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRepositoryHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookKey', (done) => {
        try {
          a.deleteRepositoryHook(null, null, null, (data, error) => {
            try {
              const displayE = 'hookKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteRepositoryHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteRepositoryHook('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteRepositoryHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteRepositoryHook('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteRepositoryHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putenableHook - errors', () => {
      it('should have a putenableHook function', (done) => {
        try {
          assert.equal(true, typeof a.putenableHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookKey', (done) => {
        try {
          a.putenableHook(null, null, null, (data, error) => {
            try {
              const displayE = 'hookKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putenableHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.putenableHook('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putenableHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.putenableHook('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putenableHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletedisableHook - errors', () => {
      it('should have a deletedisableHook function', (done) => {
        try {
          assert.equal(true, typeof a.deletedisableHook === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hookKey', (done) => {
        try {
          a.deletedisableHook(null, null, null, (data, error) => {
            try {
              const displayE = 'hookKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deletedisableHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deletedisableHook('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deletedisableHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deletedisableHook('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deletedisableHook', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeploymentCapabilities - errors', () => {
      it('should have a getDeploymentCapabilities function', (done) => {
        try {
          assert.equal(true, typeof a.getDeploymentCapabilities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetPullRequestSettings - errors', () => {
      it('should have a getgetPullRequestSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getgetPullRequestSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scmId', (done) => {
        try {
          a.getgetPullRequestSettings(null, null, (data, error) => {
            try {
              const displayE = 'scmId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetPullRequestSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetPullRequestSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetPullRequestSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postupdatePullRequestSettings - errors', () => {
      it('should have a postupdatePullRequestSettings function', (done) => {
        try {
          assert.equal(true, typeof a.postupdatePullRequestSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scmId', (done) => {
        try {
          a.postupdatePullRequestSettings(null, null, null, (data, error) => {
            try {
              const displayE = 'scmId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postupdatePullRequestSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.postupdatePullRequestSettings('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postupdatePullRequestSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postupdatePullRequestSettings('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postupdatePullRequestSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTag - errors', () => {
      it('should have a createTag function', (done) => {
        try {
          assert.equal(true, typeof a.createTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.createTag(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-createTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.createTag('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-createTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTag('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-createTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTags - errors', () => {
      it('should have a getTags function', (done) => {
        try {
          assert.equal(true, typeof a.getTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getTags('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getTags('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTag - errors', () => {
      it('should have a getTag function', (done) => {
        try {
          assert.equal(true, typeof a.getTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getTag(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getTag('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getTag('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllLabelsForRepository - errors', () => {
      it('should have a getAllLabelsForRepository function', (done) => {
        try {
          assert.equal(true, typeof a.getAllLabelsForRepository === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getAllLabelsForRepository(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getAllLabelsForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getAllLabelsForRepository('fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getAllLabelsForRepository', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addLabel - errors', () => {
      it('should have a addLabel function', (done) => {
        try {
          assert.equal(true, typeof a.addLabel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.addLabel(null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-addLabel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.addLabel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-addLabel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addLabel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-addLabel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeLabel - errors', () => {
      it('should have a removeLabel function', (done) => {
        try {
          assert.equal(true, typeof a.removeLabel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labelName', (done) => {
        try {
          a.removeLabel(null, null, null, (data, error) => {
            try {
              const displayE = 'labelName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-removeLabel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.removeLabel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-removeLabel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.removeLabel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-removeLabel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposCommitsDeployments - errors', () => {
      it('should have a getReposCommitsDeployments function', (done) => {
        try {
          assert.equal(true, typeof a.getReposCommitsDeployments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.getReposCommitsDeployments('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsDeployments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposCommitsDeployments('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsDeployments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposCommitsDeployments('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsDeployments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReposCommitsDeployments - errors', () => {
      it('should have a deleteReposCommitsDeployments function', (done) => {
        try {
          assert.equal(true, typeof a.deleteReposCommitsDeployments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.deleteReposCommitsDeployments('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposCommitsDeployments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteReposCommitsDeployments('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposCommitsDeployments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteReposCommitsDeployments('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposCommitsDeployments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postReposCommitsDeployments - errors', () => {
      it('should have a postReposCommitsDeployments function', (done) => {
        try {
          assert.equal(true, typeof a.postReposCommitsDeployments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.postReposCommitsDeployments(null, null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposCommitsDeployments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.postReposCommitsDeployments('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposCommitsDeployments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.postReposCommitsDeployments('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposCommitsDeployments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postReposCommitsDeployments('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postReposCommitsDeployments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getstreamPatch - errors', () => {
      it('should have a getstreamPatch function', (done) => {
        try {
          assert.equal(true, typeof a.getstreamPatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getstreamPatch('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstreamPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getstreamPatch('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstreamPatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdminUsers - errors', () => {
      it('should have a getAdminUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getAdminUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser - errors', () => {
      it('should have a deleteUser function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putAdminUsers - errors', () => {
      it('should have a putAdminUsers function', (done) => {
        try {
          assert.equal(true, typeof a.putAdminUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putAdminUsers(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putAdminUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createUser - errors', () => {
      it('should have a createUser function', (done) => {
        try {
          assert.equal(true, typeof a.createUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eraseUser - errors', () => {
      it('should have a eraseUser function', (done) => {
        try {
          assert.equal(true, typeof a.eraseUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateErasable - errors', () => {
      it('should have a validateErasable function', (done) => {
        try {
          assert.equal(true, typeof a.validateErasable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#renameUser - errors', () => {
      it('should have a renameUser function', (done) => {
        try {
          assert.equal(true, typeof a.renameUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.renameUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-renameUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putAdminUsersCredentials - errors', () => {
      it('should have a putAdminUsersCredentials function', (done) => {
        try {
          assert.equal(true, typeof a.putAdminUsersCredentials === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putAdminUsersCredentials(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putAdminUsersCredentials', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearUserCaptchaChallenge - errors', () => {
      it('should have a clearUserCaptchaChallenge function', (done) => {
        try {
          assert.equal(true, typeof a.clearUserCaptchaChallenge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroup - errors', () => {
      it('should have a deleteGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createGroup - errors', () => {
      it('should have a createGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdminGroups - errors', () => {
      it('should have a getAdminGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getAdminGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addUserToGroup - errors', () => {
      it('should have a addUserToGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addUserToGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addUserToGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-addUserToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addGroupToUser - errors', () => {
      it('should have a addGroupToUser function', (done) => {
        try {
          assert.equal(true, typeof a.addGroupToUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addGroupToUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-addGroupToUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addUsersToGroup - errors', () => {
      it('should have a addUsersToGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addUsersToGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addUsersToGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-addUsersToGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addUserToGroups - errors', () => {
      it('should have a addUserToGroups function', (done) => {
        try {
          assert.equal(true, typeof a.addUserToGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addUserToGroups(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-addUserToGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeUserFromGroup - errors', () => {
      it('should have a removeUserFromGroup function', (done) => {
        try {
          assert.equal(true, typeof a.removeUserFromGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.removeUserFromGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-removeUserFromGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeGroupFromUser - errors', () => {
      it('should have a removeGroupFromUser function', (done) => {
        try {
          assert.equal(true, typeof a.removeGroupFromUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.removeGroupFromUser(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-removeGroupFromUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findUsersInGroup - errors', () => {
      it('should have a findUsersInGroup function', (done) => {
        try {
          assert.equal(true, typeof a.findUsersInGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findUsersNotInGroup - errors', () => {
      it('should have a findUsersNotInGroup function', (done) => {
        try {
          assert.equal(true, typeof a.findUsersNotInGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findGroupsForUser - errors', () => {
      it('should have a findGroupsForUser function', (done) => {
        try {
          assert.equal(true, typeof a.findGroupsForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#findOtherGroupsForUser - errors', () => {
      it('should have a findOtherGroupsForUser function', (done) => {
        try {
          assert.equal(true, typeof a.findOtherGroupsForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdminLicense - errors', () => {
      it('should have a getAdminLicense function', (done) => {
        try {
          assert.equal(true, typeof a.getAdminLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postupdate - errors', () => {
      it('should have a postupdate function', (done) => {
        try {
          assert.equal(true, typeof a.postupdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postupdate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postupdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSettingsAutoDecline - errors', () => {
      it('should have a getSettingsAutoDecline function', (done) => {
        try {
          assert.equal(true, typeof a.getSettingsAutoDecline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getSettingsAutoDecline(null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getSettingsAutoDecline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSettingsAutoDecline - errors', () => {
      it('should have a deleteSettingsAutoDecline function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSettingsAutoDecline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteSettingsAutoDecline(null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteSettingsAutoDecline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSettingsAutoDecline - errors', () => {
      it('should have a putSettingsAutoDecline function', (done) => {
        try {
          assert.equal(true, typeof a.putSettingsAutoDecline === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.putSettingsAutoDecline(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putSettingsAutoDecline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putSettingsAutoDecline('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putSettingsAutoDecline', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTask - errors', () => {
      it('should have a createTask function', (done) => {
        try {
          assert.equal(true, typeof a.createTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTask(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-createTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTask - errors', () => {
      it('should have a deleteTask function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.deleteTask(null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTask - errors', () => {
      it('should have a updateTask function', (done) => {
        try {
          assert.equal(true, typeof a.updateTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.updateTask(null, null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTask('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-updateTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTask - errors', () => {
      it('should have a getTask function', (done) => {
        try {
          assert.equal(true, typeof a.getTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing taskId', (done) => {
        try {
          a.getTask(null, (data, error) => {
            try {
              const displayE = 'taskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroups - errors', () => {
      it('should have a getGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposPermissionsGroups - errors', () => {
      it('should have a getReposPermissionsGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getReposPermissionsGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposPermissionsGroups('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPermissionsGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposPermissionsGroups('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPermissionsGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReposPermissionsGroups - errors', () => {
      it('should have a deleteReposPermissionsGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteReposPermissionsGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteReposPermissionsGroups('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPermissionsGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteReposPermissionsGroups('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPermissionsGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setPermissionForGroup - errors', () => {
      it('should have a setPermissionForGroup function', (done) => {
        try {
          assert.equal(true, typeof a.setPermissionForGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.setPermissionForGroup('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setPermissionForGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.setPermissionForGroup('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setPermissionForGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetGroupsWithoutAnyPermission1 - errors', () => {
      it('should have a getgetGroupsWithoutAnyPermission1 function', (done) => {
        try {
          assert.equal(true, typeof a.getgetGroupsWithoutAnyPermission1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetGroupsWithoutAnyPermission1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetGroupsWithoutAnyPermission1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getgetGroupsWithoutAnyPermission1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetGroupsWithoutAnyPermission1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposPermissionsUsers - errors', () => {
      it('should have a getReposPermissionsUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getReposPermissionsUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposPermissionsUsers('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPermissionsUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposPermissionsUsers('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposPermissionsUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteReposPermissionsUsers - errors', () => {
      it('should have a deleteReposPermissionsUsers function', (done) => {
        try {
          assert.equal(true, typeof a.deleteReposPermissionsUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteReposPermissionsUsers('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPermissionsUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deleteReposPermissionsUsers('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteReposPermissionsUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setPermissionForUser - errors', () => {
      it('should have a setPermissionForUser function', (done) => {
        try {
          assert.equal(true, typeof a.setPermissionForUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.setPermissionForUser('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setPermissionForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.setPermissionForUser('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setPermissionForUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetUsersWithoutPermission - errors', () => {
      it('should have a getgetUsersWithoutPermission function', (done) => {
        try {
          assert.equal(true, typeof a.getgetUsersWithoutPermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetUsersWithoutPermission('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetUsersWithoutPermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getgetUsersWithoutPermission('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetUsersWithoutPermission', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetRepositories - errors', () => {
      it('should have a getgetRepositories function', (done) => {
        try {
          assert.equal(true, typeof a.getgetRepositories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stream - errors', () => {
      it('should have a stream function', (done) => {
        try {
          assert.equal(true, typeof a.stream === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.stream('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-stream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.stream('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-stream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getstream - errors', () => {
      it('should have a getstream function', (done) => {
        try {
          assert.equal(true, typeof a.getstream === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getstream('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getstream('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing path', (done) => {
        try {
          a.getstream('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'uriPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getstream', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetCommits - errors', () => {
      it('should have a getgetCommits function', (done) => {
        try {
          assert.equal(true, typeof a.getgetCommits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pullRequestId', (done) => {
        try {
          a.getgetCommits('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pullRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetCommits('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getgetCommits('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetCommits', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplicationProperties - errors', () => {
      it('should have a getApplicationProperties function', (done) => {
        try {
          assert.equal(true, typeof a.getApplicationProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetReviewerGroups - errors', () => {
      it('should have a getgetReviewerGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getgetReviewerGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetReviewerGroups(null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetReviewerGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSettingsReviewerGroups - errors', () => {
      it('should have a postSettingsReviewerGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postSettingsReviewerGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.postSettingsReviewerGroups(null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postSettingsReviewerGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postSettingsReviewerGroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postSettingsReviewerGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetReviewerGroup - errors', () => {
      it('should have a getgetReviewerGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getgetReviewerGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getgetReviewerGroup(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetReviewerGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetReviewerGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetReviewerGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSettingsReviewerGroupsWithid - errors', () => {
      it('should have a putSettingsReviewerGroupsWithid function', (done) => {
        try {
          assert.equal(true, typeof a.putSettingsReviewerGroupsWithid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.putSettingsReviewerGroupsWithid(null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putSettingsReviewerGroupsWithid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.putSettingsReviewerGroupsWithid('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putSettingsReviewerGroupsWithid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putSettingsReviewerGroupsWithid('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putSettingsReviewerGroupsWithid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSettingsReviewerGroupsWithid - errors', () => {
      it('should have a deleteSettingsReviewerGroupsWithid function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSettingsReviewerGroupsWithid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteSettingsReviewerGroupsWithid(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteSettingsReviewerGroupsWithid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deleteSettingsReviewerGroupsWithid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deleteSettingsReviewerGroupsWithid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postcreateComment - errors', () => {
      it('should have a postcreateComment function', (done) => {
        try {
          assert.equal(true, typeof a.postcreateComment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.postcreateComment('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postcreateComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.postcreateComment('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postcreateComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.postcreateComment('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postcreateComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postcreateComment('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-postcreateComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReposCommitsComments - errors', () => {
      it('should have a getReposCommitsComments function', (done) => {
        try {
          assert.equal(true, typeof a.getReposCommitsComments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.getReposCommitsComments('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getReposCommitsComments('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getReposCommitsComments('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getReposCommitsComments', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletedeleteComment - errors', () => {
      it('should have a deletedeleteComment function', (done) => {
        try {
          assert.equal(true, typeof a.deletedeleteComment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.deletedeleteComment('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deletedeleteComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.deletedeleteComment('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deletedeleteComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.deletedeleteComment('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deletedeleteComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.deletedeleteComment('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-deletedeleteComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putupdateComment - errors', () => {
      it('should have a putupdateComment function', (done) => {
        try {
          assert.equal(true, typeof a.putupdateComment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.putupdateComment(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putupdateComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.putupdateComment('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putupdateComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.putupdateComment('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putupdateComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.putupdateComment('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putupdateComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.putupdateComment('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-putupdateComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getgetComment - errors', () => {
      it('should have a getgetComment function', (done) => {
        try {
          assert.equal(true, typeof a.getgetComment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commentId', (done) => {
        try {
          a.getgetComment(null, null, null, null, (data, error) => {
            try {
              const displayE = 'commentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing commitId', (done) => {
        try {
          a.getgetComment('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'commitId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getgetComment('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getgetComment('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getgetComment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setMailConfig - errors', () => {
      it('should have a setMailConfig function', (done) => {
        try {
          assert.equal(true, typeof a.setMailConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setMailConfig(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setMailConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMailConfig - errors', () => {
      it('should have a getMailConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getMailConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMailConfig - errors', () => {
      it('should have a deleteMailConfig function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMailConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSenderAddress - errors', () => {
      it('should have a getSenderAddress function', (done) => {
        try {
          assert.equal(true, typeof a.getSenderAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearSenderAddress - errors', () => {
      it('should have a clearSenderAddress function', (done) => {
        try {
          assert.equal(true, typeof a.clearSenderAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setSenderAddress - errors', () => {
      it('should have a setSenderAddress function', (done) => {
        try {
          assert.equal(true, typeof a.setSenderAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.setSenderAddress(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-setSenderAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getArchive - errors', () => {
      it('should have a getArchive function', (done) => {
        try {
          assert.equal(true, typeof a.getArchive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing projectKey', (done) => {
        try {
          a.getArchive('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'projectKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getArchive', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing repositorySlug', (done) => {
        try {
          a.getArchive('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'repositorySlug is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-bitbucket_server-adapter-getArchive', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
