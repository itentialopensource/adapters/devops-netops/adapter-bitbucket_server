
## 1.2.4 [10-15-2024]

* Changes made at 2024.10.14_20:02PM

See merge request itentialopensource/adapters/adapter-bitbucket_server!22

---

## 1.2.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-bitbucket_server!20

---

## 1.2.2 [08-14-2024]

* Changes made at 2024.08.14_18:10PM

See merge request itentialopensource/adapters/adapter-bitbucket_server!19

---

## 1.2.1 [08-07-2024]

* Changes made at 2024.08.06_19:23PM

See merge request itentialopensource/adapters/adapter-bitbucket_server!18

---

## 1.2.0 [08-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!17

---

## 1.1.9 [03-26-2024]

* Changes made at 2024.03.26_14:22PM

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!16

---

## 1.1.8 [03-21-2024]

* Changes made at 2024.03.21_14:31PM

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!15

---

## 1.1.7 [03-11-2024]

* Changes made at 2024.03.11_13:21PM

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!14

---

## 1.1.6 [02-26-2024]

* Changes made at 2024.02.26_13:45PM

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!13

---

## 1.1.5 [12-26-2023]

* update metadata

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!12

---

## 1.1.4 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!11

---

## 1.1.3 [12-14-2023]

* Handle empty source commit id

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!10

---

## 1.1.2 [11-21-2023]

* Handle empty source commit id

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!10

---

## 1.1.1 [11-20-2023]

* Handle empty source commit id

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!10

---

## 1.1.0 [11-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!8

---

## 1.0.3 [05-30-2023]

* Add get file streams with query

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!7

---

## 1.0.2 [03-09-2023]

* ADAPT-2560-Add support for editting multiple files

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!6

---

## 1.0.1 [02-27-2023]

* Update getgetcontent task to take in query options

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!5

---

## 1.0.0 [09-09-2022]

* Update call names

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!4

---

## 0.1.2 [07-25-2022]

* Update action.json with the correct entitypath for healthcheck

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!3

---

## 0.1.1 [06-24-2022]

* Fixed editFile task to allow file upload.

See merge request itentialopensource/adapters/devops-netops/adapter-bitbucket_server!1

---
