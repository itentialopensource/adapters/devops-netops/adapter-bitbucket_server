# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Bitbucket_server System. The API that was used to build the adapter for Bitbucket_server is usually available in the report directory of this adapter. The adapter utilizes the Bitbucket_server API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Bitbucket Server adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Bitbucket Server. With this adapter you have the ability to perform operations such as:

- Create, publish, maintain, update repositories in Bitbucket.

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
